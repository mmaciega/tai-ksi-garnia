# TAI Księgarnia #

Aplikacja służy do zarządzania księgarnią. Aplikacja została stworzona w języku programowania Java. Interfejs graficzny został stworzony przy użyciu technologii JavaFX. Dane programu przechowywane są w relacyjnej bazie danych H2. W celu uzyskania dostępu do danych zawartych w bazie wykorzystany został framework Hibernate.