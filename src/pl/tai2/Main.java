package pl.tai2;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import pl.tai2.db.DbManager;
import pl.tai2.view.GlowneOknoController;

public class Main extends Application {
    private final String NAZWA_APLIKACJI = "Program księgarnia - TAI Projekt nr 2";
    
    private Stage glownaScena;
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.glownaScena = primaryStage;
        
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/GlowneOkno.fxml"));
        AnchorPane glownyPanel = (AnchorPane) loader.load();
        
        Scene scene = new Scene(glownyPanel);
        this.getGlownaScena().setScene(scene);
        this.getGlownaScena().setTitle(NAZWA_APLIKACJI);
        this.getGlownaScena().show();
        this.getGlownaScena().setOnCloseRequest((WindowEvent event) -> { 
            DbManager.getInstance().shutDown();
            Platform.exit();  System.exit(0); });
        
        
        GlowneOknoController kontroler = loader.getController();
        kontroler.setMainApp(this);
    }

    public static void main(String[] args) { 
        DbManager.getInstance();
        launch(args);
    }

    public Stage getGlownaScena() {
        return glownaScena;
    }
    
}
