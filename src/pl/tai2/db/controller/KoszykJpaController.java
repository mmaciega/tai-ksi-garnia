package pl.tai2.db.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pl.tai2.db.model.Klient;
import pl.tai2.db.model.ProduktWKoszyku;
import java.util.ArrayList;
import java.util.List;
import pl.tai2.db.model.Sprzedaz;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.EntityManager;
import pl.tai2.db.DbManager;
import pl.tai2.db.controller.exceptions.IllegalOrphanException;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.model.Koszyk;

/**
 *
 * @author Mateusz Macięga
 */
public class KoszykJpaController implements Serializable {

    private final EntityManager em;

    public KoszykJpaController() {
        this.em = DbManager.getInstance().getEntityManager();
    }

    public void create(Koszyk koszyk) {
        if (koszyk.getProduktWKoszykus() == null) {
            koszyk.setProduktWKoszykus(new ArrayList<>());
        }
        if (koszyk.getSprzedazs() == null) {
            koszyk.setSprzedazs(new HashSet<>());
        }
        em.getTransaction().begin();
        Klient klient = koszyk.getKlient();
        if (klient != null) {
            klient = em.getReference(Klient.class, klient.getIdKlienta());
            koszyk.setKlient(klient);
        }
        List<ProduktWKoszyku> attachedProduktWKoszykus = new ArrayList<ProduktWKoszyku>();
        for (ProduktWKoszyku produktWKoszykusProduktWKoszykuToAttach : koszyk.getProduktWKoszykus()) {
            produktWKoszykusProduktWKoszykuToAttach = em.getReference(ProduktWKoszyku.class, produktWKoszykusProduktWKoszykuToAttach.getId());
            attachedProduktWKoszykus.add(produktWKoszykusProduktWKoszykuToAttach);
        }
        koszyk.setProduktWKoszykus(attachedProduktWKoszykus);
        Set<Sprzedaz> attachedSprzedazs = new HashSet<Sprzedaz>();
        for (Sprzedaz sprzedazsSprzedazToAttach : koszyk.getSprzedazs()) {
            sprzedazsSprzedazToAttach = em.getReference(Sprzedaz.class, sprzedazsSprzedazToAttach.getIdSprzedazy());
            attachedSprzedazs.add(sprzedazsSprzedazToAttach);
        }
        koszyk.setSprzedazs(attachedSprzedazs);
        em.persist(koszyk);
        if (klient != null) {
            klient.getKoszyks().add(koszyk);
            klient = em.merge(klient);
        }
        for (ProduktWKoszyku produktWKoszykusProduktWKoszyku : koszyk.getProduktWKoszykus()) {
            Koszyk oldKoszykOfProduktWKoszykusProduktWKoszyku = produktWKoszykusProduktWKoszyku.getKoszyk();
            produktWKoszykusProduktWKoszyku.setKoszyk(koszyk);
            produktWKoszykusProduktWKoszyku = em.merge(produktWKoszykusProduktWKoszyku);
            if (oldKoszykOfProduktWKoszykusProduktWKoszyku != null) {
                oldKoszykOfProduktWKoszykusProduktWKoszyku.getProduktWKoszykus().remove(produktWKoszykusProduktWKoszyku);
                oldKoszykOfProduktWKoszykusProduktWKoszyku = em.merge(oldKoszykOfProduktWKoszykusProduktWKoszyku);
            }
        }
        for (Sprzedaz sprzedazsSprzedaz : koszyk.getSprzedazs()) {
            Koszyk oldKoszykOfSprzedazsSprzedaz = sprzedazsSprzedaz.getKoszyk();
            sprzedazsSprzedaz.setKoszyk(koszyk);
            sprzedazsSprzedaz = em.merge(sprzedazsSprzedaz);
            if (oldKoszykOfSprzedazsSprzedaz != null) {
                oldKoszykOfSprzedazsSprzedaz.getSprzedazs().remove(sprzedazsSprzedaz);
                oldKoszykOfSprzedazsSprzedaz = em.merge(oldKoszykOfSprzedazsSprzedaz);
            }
        }
        em.getTransaction().commit();
    }

    public void edit(Koszyk koszyk) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            em.getTransaction().begin();
            Koszyk persistentKoszyk = em.find(Koszyk.class, koszyk.getIdKoszyka());
            Klient klientOld = persistentKoszyk.getKlient();
            Klient klientNew = koszyk.getKlient();
            List<ProduktWKoszyku> produktWKoszykusOld = persistentKoszyk.getProduktWKoszykus();
            List<ProduktWKoszyku> produktWKoszykusNew = koszyk.getProduktWKoszykus();
            Set<Sprzedaz> sprzedazsOld = persistentKoszyk.getSprzedazs();
            Set<Sprzedaz> sprzedazsNew = koszyk.getSprzedazs();
            List<String> illegalOrphanMessages = null;
            for (ProduktWKoszyku produktWKoszykusOldProduktWKoszyku : produktWKoszykusOld) {
                if (!produktWKoszykusNew.contains(produktWKoszykusOldProduktWKoszyku)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ProduktWKoszyku " + produktWKoszykusOldProduktWKoszyku + " since its koszyk field is not nullable.");
                }
            }
            for (Sprzedaz sprzedazsOldSprzedaz : sprzedazsOld) {
                if (!sprzedazsNew.contains(sprzedazsOldSprzedaz)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Sprzedaz " + sprzedazsOldSprzedaz + " since its koszyk field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (klientNew != null) {
                klientNew = em.getReference(Klient.class, klientNew.getIdKlienta());
                koszyk.setKlient(klientNew);
            }
            List<ProduktWKoszyku> attachedProduktWKoszykusNew = new ArrayList<ProduktWKoszyku>();
            for (ProduktWKoszyku produktWKoszykusNewProduktWKoszykuToAttach : produktWKoszykusNew) {
                produktWKoszykusNewProduktWKoszykuToAttach = em.getReference(ProduktWKoszyku.class, produktWKoszykusNewProduktWKoszykuToAttach.getId());
                attachedProduktWKoszykusNew.add(produktWKoszykusNewProduktWKoszykuToAttach);
            }
            produktWKoszykusNew = attachedProduktWKoszykusNew;
            koszyk.setProduktWKoszykus(produktWKoszykusNew);
            Set<Sprzedaz> attachedSprzedazsNew = new HashSet<Sprzedaz>();
            for (Sprzedaz sprzedazsNewSprzedazToAttach : sprzedazsNew) {
                sprzedazsNewSprzedazToAttach = em.getReference(Sprzedaz.class, sprzedazsNewSprzedazToAttach.getIdSprzedazy());
                attachedSprzedazsNew.add(sprzedazsNewSprzedazToAttach);
            }
            sprzedazsNew = attachedSprzedazsNew;
            koszyk.setSprzedazs(sprzedazsNew);
            koszyk = em.merge(koszyk);
            if (klientOld != null && !klientOld.equals(klientNew)) {
                klientOld.getKoszyks().remove(koszyk);
                klientOld = em.merge(klientOld);
            }
            if (klientNew != null && !klientNew.equals(klientOld)) {
                klientNew.getKoszyks().add(koszyk);
                klientNew = em.merge(klientNew);
            }
            for (ProduktWKoszyku produktWKoszykusNewProduktWKoszyku : produktWKoszykusNew) {
                if (!produktWKoszykusOld.contains(produktWKoszykusNewProduktWKoszyku)) {
                    Koszyk oldKoszykOfProduktWKoszykusNewProduktWKoszyku = produktWKoszykusNewProduktWKoszyku.getKoszyk();
                    produktWKoszykusNewProduktWKoszyku.setKoszyk(koszyk);
                    produktWKoszykusNewProduktWKoszyku = em.merge(produktWKoszykusNewProduktWKoszyku);
                    if (oldKoszykOfProduktWKoszykusNewProduktWKoszyku != null && !oldKoszykOfProduktWKoszykusNewProduktWKoszyku.equals(koszyk)) {
                        oldKoszykOfProduktWKoszykusNewProduktWKoszyku.getProduktWKoszykus().remove(produktWKoszykusNewProduktWKoszyku);
                        oldKoszykOfProduktWKoszykusNewProduktWKoszyku = em.merge(oldKoszykOfProduktWKoszykusNewProduktWKoszyku);
                    }
                }
            }
            for (Sprzedaz sprzedazsNewSprzedaz : sprzedazsNew) {
                if (!sprzedazsOld.contains(sprzedazsNewSprzedaz)) {
                    Koszyk oldKoszykOfSprzedazsNewSprzedaz = sprzedazsNewSprzedaz.getKoszyk();
                    sprzedazsNewSprzedaz.setKoszyk(koszyk);
                    sprzedazsNewSprzedaz = em.merge(sprzedazsNewSprzedaz);
                    if (oldKoszykOfSprzedazsNewSprzedaz != null && !oldKoszykOfSprzedazsNewSprzedaz.equals(koszyk)) {
                        oldKoszykOfSprzedazsNewSprzedaz.getSprzedazs().remove(sprzedazsNewSprzedaz);
                        oldKoszykOfSprzedazsNewSprzedaz = em.merge(oldKoszykOfSprzedazsNewSprzedaz);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = koszyk.getIdKoszyka();
                if (findKoszyk(id) == null) {
                    throw new NonexistentEntityException("The koszyk with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        em.getTransaction().begin();
        Koszyk koszyk;
        try {
            koszyk = em.getReference(Koszyk.class, id);
            koszyk.getIdKoszyka();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The koszyk with id " + id + " no longer exists.", enfe);
        }
        List<String> illegalOrphanMessages = null;
        Set<Sprzedaz> sprzedazsOrphanCheck = koszyk.getSprzedazs();
        for (Sprzedaz sprzedazsOrphanCheckSprzedaz : sprzedazsOrphanCheck) {
            if (illegalOrphanMessages == null) {
                illegalOrphanMessages = new ArrayList<String>();
            }
            illegalOrphanMessages.add("This Koszyk (" + koszyk + ") cannot be destroyed since the Sprzedaz " + sprzedazsOrphanCheckSprzedaz + " in its sprzedazs field has a non-nullable koszyk field.");
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        Klient klient = koszyk.getKlient();
        if (klient != null) {
            klient.getKoszyks().remove(koszyk);
            klient = em.merge(klient);
        }
        em.remove(koszyk);
        em.getTransaction().commit();
    }

    public List<Koszyk> findKoszykEntities() {
        return findKoszykEntities(true, -1, -1);
    }

    public List<Koszyk> findKoszykEntities(int maxResults, int firstResult) {
        return findKoszykEntities(false, maxResults, firstResult);
    }

    private List<Koszyk> findKoszykEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Koszyk.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Koszyk findKoszyk(Long id) {
        return em.find(Koszyk.class, id);
    }

    public int getKoszykCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Koszyk> rt = cq.from(Koszyk.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
