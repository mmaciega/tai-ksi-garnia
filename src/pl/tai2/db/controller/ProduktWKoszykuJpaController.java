package pl.tai2.db.controller;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pl.tai2.db.DbManager;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.controller.exceptions.PreexistingEntityException;
import pl.tai2.db.model.Koszyk;
import pl.tai2.db.model.ProduktWKoszyku;
import pl.tai2.db.model.ProduktWKoszykuId;

/**
 *
 * @author Mateusz Macięga
 */
public class ProduktWKoszykuJpaController implements Serializable {

    private final EntityManager em;

    public ProduktWKoszykuJpaController() {
        this.em = DbManager.getInstance().getEntityManager();
    }

    public void create(ProduktWKoszyku produktWKoszyku) throws PreexistingEntityException, Exception {
        if (produktWKoszyku.getId() == null) {
            produktWKoszyku.setId(new ProduktWKoszykuId());
        }
        try {
            em.getTransaction().begin();
            Koszyk koszyk = produktWKoszyku.getKoszyk();
            if (koszyk != null) {
                koszyk = em.getReference(Koszyk.class, koszyk.getIdKoszyka());
                produktWKoszyku.setKoszyk(koszyk);
            }
            em.persist(produktWKoszyku);
            if (koszyk != null) {
                koszyk.getProduktWKoszykus().add(produktWKoszyku);
                koszyk = em.merge(koszyk);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProduktWKoszyku(produktWKoszyku.getId()) != null) {
                throw new PreexistingEntityException("ProduktWKoszyku " + produktWKoszyku + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ProduktWKoszyku produktWKoszyku) throws NonexistentEntityException, Exception {
        try {
            em.getTransaction().begin();
            ProduktWKoszyku persistentProduktWKoszyku = em.find(ProduktWKoszyku.class, produktWKoszyku.getId());
            Koszyk koszykOld = persistentProduktWKoszyku.getKoszyk();
            Koszyk koszykNew = produktWKoszyku.getKoszyk();
            if (koszykNew != null) {
                koszykNew = em.getReference(Koszyk.class, koszykNew.getIdKoszyka());
                produktWKoszyku.setKoszyk(koszykNew);
            }
            produktWKoszyku = em.merge(produktWKoszyku);
            if (koszykOld != null && !koszykOld.equals(koszykNew)) {
                koszykOld.getProduktWKoszykus().remove(produktWKoszyku);
                koszykOld = em.merge(koszykOld);
            }
            if (koszykNew != null && !koszykNew.equals(koszykOld)) {
                koszykNew.getProduktWKoszykus().add(produktWKoszyku);
                koszykNew = em.merge(koszykNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ProduktWKoszykuId id = produktWKoszyku.getId();
                if (findProduktWKoszyku(id) == null) {
                    throw new NonexistentEntityException("The produktWKoszyku with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(ProduktWKoszykuId id) throws NonexistentEntityException {
        em.getTransaction().begin();
        ProduktWKoszyku produktWKoszyku;
        try {
            produktWKoszyku = em.getReference(ProduktWKoszyku.class, id);
            produktWKoszyku.getId();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The produktWKoszyku with id " + id + " no longer exists.", enfe);
        }
        Koszyk koszyk = produktWKoszyku.getKoszyk();
        if (koszyk != null) {
            koszyk.getProduktWKoszykus().remove(produktWKoszyku);
            koszyk = em.merge(koszyk);
        }
        em.remove(produktWKoszyku);
        em.getTransaction().commit();
    }

    public List<ProduktWKoszyku> findProduktWKoszykuEntities() {
        return findProduktWKoszykuEntities(true, -1, -1);
    }

    public List<ProduktWKoszyku> findProduktWKoszykuEntities(int maxResults, int firstResult) {
        return findProduktWKoszykuEntities(false, maxResults, firstResult);
    }

    private List<ProduktWKoszyku> findProduktWKoszykuEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ProduktWKoszyku.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public ProduktWKoszyku findProduktWKoszyku(ProduktWKoszykuId id) {

        return em.find(ProduktWKoszyku.class, id);
    }

    public int getProduktWKoszykuCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ProduktWKoszyku> rt = cq.from(ProduktWKoszyku.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
