package pl.tai2.db.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pl.tai2.db.model.Ksiazka;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import pl.tai2.db.DbManager;
import pl.tai2.db.controller.exceptions.IllegalOrphanException;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.model.Wydawnictwo;

/**
 *
 * @author Mateusz Macięga
 */
public class WydawnictwoJpaController implements Serializable {

    private final EntityManager em;

    public WydawnictwoJpaController() {
        this.em = DbManager.getInstance().getEntityManager();
    }

    public void create(Wydawnictwo wydawnictwo) {
        if (wydawnictwo.getKsiazkaList() == null) {
            wydawnictwo.setKsiazkaList(new ArrayList<>());
        }
        em.getTransaction().begin();
        List<Ksiazka> attachedKsiazkaList = new ArrayList<Ksiazka>();
        for (Ksiazka ksiazkaListKsiazkaToAttach : wydawnictwo.getKsiazkaList()) {
            ksiazkaListKsiazkaToAttach = em.getReference(ksiazkaListKsiazkaToAttach.getClass(), ksiazkaListKsiazkaToAttach.getIdKsiazki());
            attachedKsiazkaList.add(ksiazkaListKsiazkaToAttach);
        }
        wydawnictwo.setKsiazkaList(attachedKsiazkaList);
        em.persist(wydawnictwo);
        for (Ksiazka ksiazkaListKsiazka : wydawnictwo.getKsiazkaList()) {
            Wydawnictwo oldWydawnictwoOfKsiazkaListKsiazka = ksiazkaListKsiazka.getWydawnictwo();
            ksiazkaListKsiazka.setWydawnictwo(wydawnictwo);
            ksiazkaListKsiazka = em.merge(ksiazkaListKsiazka);
            if (oldWydawnictwoOfKsiazkaListKsiazka != null) {
                oldWydawnictwoOfKsiazkaListKsiazka.getKsiazkaList().remove(ksiazkaListKsiazka);
                oldWydawnictwoOfKsiazkaListKsiazka = em.merge(oldWydawnictwoOfKsiazkaListKsiazka);
            }
        }
        em.getTransaction().commit();
    }

    public void edit(Wydawnictwo wydawnictwo) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            em.getTransaction().begin();
            Wydawnictwo persistentWydawnictwo = em.find(Wydawnictwo.class, wydawnictwo.getIdWydawnictwa());
            List<Ksiazka> ksiazkaListOld = persistentWydawnictwo.getKsiazkaList();
            List<Ksiazka> ksiazkaListNew = wydawnictwo.getKsiazkaList();
            List<String> illegalOrphanMessages = null;
            for (Ksiazka ksiazkaListOldKsiazka : ksiazkaListOld) {
                if (!ksiazkaListNew.contains(ksiazkaListOldKsiazka)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ksiazka " + ksiazkaListOldKsiazka + " since its wydawnictwo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Ksiazka> attachedKsiazkaListNew = new ArrayList<Ksiazka>();
            for (Ksiazka ksiazkaListNewKsiazkaToAttach : ksiazkaListNew) {
                ksiazkaListNewKsiazkaToAttach = em.getReference(ksiazkaListNewKsiazkaToAttach.getClass(), ksiazkaListNewKsiazkaToAttach.getIdKsiazki());
                attachedKsiazkaListNew.add(ksiazkaListNewKsiazkaToAttach);
            }
            ksiazkaListNew = attachedKsiazkaListNew;
            wydawnictwo.setKsiazkaList(ksiazkaListNew);
            wydawnictwo = em.merge(wydawnictwo);
            for (Ksiazka ksiazkaListNewKsiazka : ksiazkaListNew) {
                if (!ksiazkaListOld.contains(ksiazkaListNewKsiazka)) {
                    Wydawnictwo oldWydawnictwoOfKsiazkaListNewKsiazka = ksiazkaListNewKsiazka.getWydawnictwo();
                    ksiazkaListNewKsiazka.setWydawnictwo(wydawnictwo);
                    ksiazkaListNewKsiazka = em.merge(ksiazkaListNewKsiazka);
                    if (oldWydawnictwoOfKsiazkaListNewKsiazka != null && !oldWydawnictwoOfKsiazkaListNewKsiazka.equals(wydawnictwo)) {
                        oldWydawnictwoOfKsiazkaListNewKsiazka.getKsiazkaList().remove(ksiazkaListNewKsiazka);
                        oldWydawnictwoOfKsiazkaListNewKsiazka = em.merge(oldWydawnictwoOfKsiazkaListNewKsiazka);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = wydawnictwo.getIdWydawnictwa();
                if (findWydawnictwo(id) == null) {
                    throw new NonexistentEntityException("The wydawnictwo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {

        em.getTransaction().begin();
        Wydawnictwo wydawnictwo;
        try {
            wydawnictwo = em.getReference(Wydawnictwo.class, id);
            wydawnictwo.getIdWydawnictwa();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The wydawnictwo with id " + id + " no longer exists.", enfe);
        }
        List<String> illegalOrphanMessages = null;
        List<Ksiazka> ksiazkaListOrphanCheck = wydawnictwo.getKsiazkaList();
        for (Ksiazka ksiazkaListOrphanCheckKsiazka : ksiazkaListOrphanCheck) {
            if (illegalOrphanMessages == null) {
                illegalOrphanMessages = new ArrayList<String>();
            }
            illegalOrphanMessages.add("This Wydawnictwo (" + wydawnictwo + ") cannot be destroyed since the Ksiazka " + ksiazkaListOrphanCheckKsiazka + " in its ksiazkaList field has a non-nullable wydawnictwo field.");
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        em.remove(wydawnictwo);
        em.getTransaction().commit();
    }

    public List<Wydawnictwo> findWydawnictwoEntities() {
        return findWydawnictwoEntities(true, -1, -1);
    }

    public List<Wydawnictwo> findWydawnictwoEntities(int maxResults, int firstResult) {
        return findWydawnictwoEntities(false, maxResults, firstResult);
    }

    private List<Wydawnictwo> findWydawnictwoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Wydawnictwo.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Wydawnictwo findWydawnictwo(Long id) {
        return em.find(Wydawnictwo.class, id);
    }

    public int getWydawnictwoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Wydawnictwo> rt = cq.from(Wydawnictwo.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
