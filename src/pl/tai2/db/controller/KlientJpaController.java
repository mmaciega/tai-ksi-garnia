package pl.tai2.db.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pl.tai2.db.model.Adres;
import pl.tai2.db.model.Koszyk;
import java.util.HashSet;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import pl.tai2.db.DbManager;
import pl.tai2.db.controller.exceptions.IllegalOrphanException;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.model.Klient;

/**
 *
 * @author Mateusz Macięga
 */
public class KlientJpaController implements Serializable {

    private final EntityManager em;

    public KlientJpaController() {
        this.em = DbManager.getInstance().getEntityManager();
    }

    public void create(Klient klient) {
        if (klient.getKoszyks() == null) {
            klient.setKoszyks(new HashSet<>());
        }

        em.getTransaction().begin();
        Adres adres = klient.getAdres();
        if (adres != null) {
            adres = em.getReference(Adres.class, adres.getIdAdresu());
            klient.setAdres(adres);
        }
        Set<Koszyk> attachedKoszyks = new HashSet<Koszyk>();
        for (Koszyk koszyksKoszykToAttach : klient.getKoszyks()) {
            koszyksKoszykToAttach = em.getReference(Koszyk.class, koszyksKoszykToAttach.getIdKoszyka());
            attachedKoszyks.add(koszyksKoszykToAttach);
        }
        klient.setKoszyks(attachedKoszyks);
        em.persist(klient);
        if (adres != null) {
            adres.getKlients().add(klient);
            adres = em.merge(adres);
        }
        for (Koszyk koszyksKoszyk : klient.getKoszyks()) {
            Klient oldKlientOfKoszyksKoszyk = koszyksKoszyk.getKlient();
            koszyksKoszyk.setKlient(klient);
            koszyksKoszyk = em.merge(koszyksKoszyk);
            if (oldKlientOfKoszyksKoszyk != null) {
                oldKlientOfKoszyksKoszyk.getKoszyks().remove(koszyksKoszyk);
                oldKlientOfKoszyksKoszyk = em.merge(oldKlientOfKoszyksKoszyk);
            }
        }
        em.getTransaction().commit();
    }

    public void edit(Klient klient) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            em.getTransaction().begin();
            Klient persistentKlient = em.find(Klient.class, klient.getIdKlienta());
            Adres adresOld = persistentKlient.getAdres();
            Adres adresNew = klient.getAdres();
            Set<Koszyk> koszyksOld = persistentKlient.getKoszyks();
            Set<Koszyk> koszyksNew = klient.getKoszyks();
            List<String> illegalOrphanMessages = null;
            for (Koszyk koszyksOldKoszyk : koszyksOld) {
                if (!koszyksNew.contains(koszyksOldKoszyk)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Koszyk " + koszyksOldKoszyk + " since its klient field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (adresNew != null) {
                adresNew = em.getReference(Adres.class, adresNew.getIdAdresu());
                klient.setAdres(adresNew);
            }
            Set<Koszyk> attachedKoszyksNew = new HashSet<Koszyk>();
            for (Koszyk koszyksNewKoszykToAttach : koszyksNew) {
                koszyksNewKoszykToAttach = em.getReference(Koszyk.class, koszyksNewKoszykToAttach.getIdKoszyka());
                attachedKoszyksNew.add(koszyksNewKoszykToAttach);
            }
            koszyksNew = attachedKoszyksNew;
            klient.setKoszyks(koszyksNew);
            klient = em.merge(klient);
            if (adresOld != null && !adresOld.equals(adresNew)) {
                adresOld.getKlients().remove(klient);
                adresOld = em.merge(adresOld);
            }
            if (adresNew != null && !adresNew.equals(adresOld)) {
                adresNew.getKlients().add(klient);
                adresNew = em.merge(adresNew);
            }
            for (Koszyk koszyksNewKoszyk : koszyksNew) {
                if (!koszyksOld.contains(koszyksNewKoszyk)) {
                    Klient oldKlientOfKoszyksNewKoszyk = koszyksNewKoszyk.getKlient();
                    koszyksNewKoszyk.setKlient(klient);
                    koszyksNewKoszyk = em.merge(koszyksNewKoszyk);
                    if (oldKlientOfKoszyksNewKoszyk != null && !oldKlientOfKoszyksNewKoszyk.equals(klient)) {
                        oldKlientOfKoszyksNewKoszyk.getKoszyks().remove(koszyksNewKoszyk);
                        oldKlientOfKoszyksNewKoszyk = em.merge(oldKlientOfKoszyksNewKoszyk);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = klient.getIdKlienta();
                if (findKlient(id) == null) {
                    throw new NonexistentEntityException("The klient with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        em.getTransaction().begin();
        Klient klient;
        try {
            klient = em.getReference(Klient.class, id);
            klient.getIdKlienta();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The klient with id " + id + " no longer exists.", enfe);
        }
//        List<String> illegalOrphanMessages = null;
//        Set<Koszyk> koszyksOrphanCheck = klient.getKoszyks();
//        for (Koszyk koszyksOrphanCheckKoszyk : koszyksOrphanCheck) {
//            if (illegalOrphanMessages == null) {
//                illegalOrphanMessages = new ArrayList<String>();
//            }
//            illegalOrphanMessages.add("This Klient (" + klient + ") cannot be destroyed since the Koszyk " + koszyksOrphanCheckKoszyk + " in its koszyks field has a non-nullable klient field.");
//        }
//        if (illegalOrphanMessages != null) {
//            throw new IllegalOrphanException(illegalOrphanMessages);
//        }
        Adres adres = klient.getAdres();
        if (adres != null) {
            adres.getKlients().remove(klient);
            adres = em.merge(adres);
        }
        em.remove(klient);
        em.getTransaction().commit();
    }

    public List<Klient> findKlientEntities() {
        return findKlientEntities(true, -1, -1);
    }

    public List<Klient> findKlientEntities(int maxResults, int firstResult) {
        return findKlientEntities(false, maxResults, firstResult);
    }

    private List<Klient> findKlientEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Klient.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Klient findKlient(Long id) {
        return em.find(Klient.class, id);
    }

    public int getKlientCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Klient> rt = cq.from(Klient.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
