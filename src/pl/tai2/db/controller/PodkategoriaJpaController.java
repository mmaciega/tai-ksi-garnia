package pl.tai2.db.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pl.tai2.db.model.Kategoria;
import pl.tai2.db.model.Ksiazka;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import pl.tai2.db.DbManager;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.model.Podkategoria;

/**
 *
 * @author Mateusz Macięga
 */
public class PodkategoriaJpaController implements Serializable {

    private final EntityManager em;

    public PodkategoriaJpaController() {
        this.em = DbManager.getInstance().getEntityManager();
    }

    public void create(Podkategoria podkategoria) {
        if (podkategoria.getKsiazkaList() == null) {
            podkategoria.setKsiazkaList(new ArrayList<>());
        }

        em.getTransaction().begin();
        Kategoria kategoria = podkategoria.getKategoria();
        if (kategoria != null) {
            kategoria = em.getReference(kategoria.getClass(), kategoria.getIdKategorii());
            podkategoria.setKategoria(kategoria);
        }
        List<Ksiazka> attachedKsiazkaList = new ArrayList<Ksiazka>();
        for (Ksiazka ksiazkaListKsiazkaToAttach : podkategoria.getKsiazkaList()) {
            ksiazkaListKsiazkaToAttach = em.getReference(ksiazkaListKsiazkaToAttach.getClass(), ksiazkaListKsiazkaToAttach.getIdKsiazki());
            attachedKsiazkaList.add(ksiazkaListKsiazkaToAttach);
        }
        podkategoria.setKsiazkaList(attachedKsiazkaList);
        em.persist(podkategoria);
        if (kategoria != null) {
            kategoria.getPodkategoriaList().add(podkategoria);
            kategoria = em.merge(kategoria);
        }
        for (Ksiazka ksiazkaListKsiazka : podkategoria.getKsiazkaList()) {
            ksiazkaListKsiazka.getPodkategoriaList().add(podkategoria);
            ksiazkaListKsiazka = em.merge(ksiazkaListKsiazka);
        }
        em.getTransaction().commit();
    }

    public void edit(Podkategoria podkategoria) throws NonexistentEntityException, Exception {
        try {
            em.getTransaction().begin();
            Podkategoria persistentPodkategoria = em.find(Podkategoria.class, podkategoria.getIdPodkategorii());
            Kategoria kategoriaOld = persistentPodkategoria.getKategoria();
            Kategoria kategoriaNew = podkategoria.getKategoria();
            List<Ksiazka> ksiazkaListOld = persistentPodkategoria.getKsiazkaList();
            List<Ksiazka> ksiazkaListNew = podkategoria.getKsiazkaList();
            if (kategoriaNew != null) {
                kategoriaNew = em.getReference(kategoriaNew.getClass(), kategoriaNew.getIdKategorii());
                podkategoria.setKategoria(kategoriaNew);
            }
            List<Ksiazka> attachedKsiazkaListNew = new ArrayList<Ksiazka>();
            for (Ksiazka ksiazkaListNewKsiazkaToAttach : ksiazkaListNew) {
                ksiazkaListNewKsiazkaToAttach = em.getReference(ksiazkaListNewKsiazkaToAttach.getClass(), ksiazkaListNewKsiazkaToAttach.getIdKsiazki());
                attachedKsiazkaListNew.add(ksiazkaListNewKsiazkaToAttach);
            }
            ksiazkaListNew = attachedKsiazkaListNew;
            podkategoria.setKsiazkaList(ksiazkaListNew);
            podkategoria = em.merge(podkategoria);
            if (kategoriaOld != null && !kategoriaOld.equals(kategoriaNew)) {
                kategoriaOld.getPodkategoriaList().remove(podkategoria);
                kategoriaOld = em.merge(kategoriaOld);
            }
            if (kategoriaNew != null && !kategoriaNew.equals(kategoriaOld)) {
                kategoriaNew.getPodkategoriaList().add(podkategoria);
                kategoriaNew = em.merge(kategoriaNew);
            }
            for (Ksiazka ksiazkaListOldKsiazka : ksiazkaListOld) {
                if (!ksiazkaListNew.contains(ksiazkaListOldKsiazka)) {
                    ksiazkaListOldKsiazka.getPodkategoriaList().remove(podkategoria);
                    ksiazkaListOldKsiazka = em.merge(ksiazkaListOldKsiazka);
                }
            }
            for (Ksiazka ksiazkaListNewKsiazka : ksiazkaListNew) {
                if (!ksiazkaListOld.contains(ksiazkaListNewKsiazka)) {
                    ksiazkaListNewKsiazka.getPodkategoriaList().add(podkategoria);
                    ksiazkaListNewKsiazka = em.merge(ksiazkaListNewKsiazka);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = podkategoria.getIdPodkategorii();
                if (findPodkategoria(id) == null) {
                    throw new NonexistentEntityException("The podkategoria with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        em.getTransaction().begin();
        Podkategoria podkategoria;
        try {
            podkategoria = em.getReference(Podkategoria.class, id);
            podkategoria.getIdPodkategorii();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The podkategoria with id " + id + " no longer exists.", enfe);
        }
        Kategoria kategoria = podkategoria.getKategoria();
        if (kategoria != null) {
            kategoria.getPodkategoriaList().remove(podkategoria);
            kategoria = em.merge(kategoria);
        }
        List<Ksiazka> ksiazkaList = podkategoria.getKsiazkaList();
        for (Ksiazka ksiazkaListKsiazka : ksiazkaList) {
            ksiazkaListKsiazka.getPodkategoriaList().remove(podkategoria);
            ksiazkaListKsiazka = em.merge(ksiazkaListKsiazka);
        }
        em.remove(podkategoria);
        em.getTransaction().commit();
    }

    public List<Podkategoria> findPodkategoriaEntities() {
        return findPodkategoriaEntities(true, -1, -1);
    }

    public List<Podkategoria> findPodkategoriaEntities(int maxResults, int firstResult) {
        return findPodkategoriaEntities(false, maxResults, firstResult);
    }

    private List<Podkategoria> findPodkategoriaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Podkategoria.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Podkategoria findPodkategoria(Long id) {
        return em.find(Podkategoria.class, id);

    }

    public int getPodkategoriaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Podkategoria> rt = cq.from(Podkategoria.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
