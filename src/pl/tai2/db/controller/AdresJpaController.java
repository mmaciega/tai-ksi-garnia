package pl.tai2.db.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pl.tai2.db.model.Klient;
import java.util.HashSet;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import pl.tai2.db.DbManager;
import pl.tai2.db.controller.exceptions.IllegalOrphanException;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.model.Adres;

/**
 *
 * @author Mateusz Macięga
 */
public class AdresJpaController implements Serializable {

    private final EntityManager em;

    public AdresJpaController() {
        this.em = DbManager.getInstance().getEntityManager();
    }

    public void create(Adres adres) {
        if (adres.getKlients() == null) {
            adres.setKlients(new HashSet<>());
        }
        em.getTransaction().begin();
        Set<Klient> attachedKlients = new HashSet<Klient>();
        for (Klient klientsKlientToAttach : adres.getKlients()) {
            klientsKlientToAttach = em.getReference(Klient.class, klientsKlientToAttach.getIdKlienta());
            attachedKlients.add(klientsKlientToAttach);
        }
        adres.setKlients(attachedKlients);
        em.persist(adres);
        for (Klient klientsKlient : adres.getKlients()) {
            Adres oldAdresOfKlientsKlient = klientsKlient.getAdres();
            klientsKlient.setAdres(adres);
            klientsKlient = em.merge(klientsKlient);
            if (oldAdresOfKlientsKlient != null) {
                oldAdresOfKlientsKlient.getKlients().remove(klientsKlient);
                oldAdresOfKlientsKlient = em.merge(oldAdresOfKlientsKlient);
            }
        }
        em.getTransaction().commit();
    }

    public void edit(Adres adres) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            em.getTransaction().begin();
            Adres persistentAdres = em.find(Adres.class, adres.getIdAdresu());
            Set<Klient> klientsOld = persistentAdres.getKlients();
            Set<Klient> klientsNew = adres.getKlients();
            List<String> illegalOrphanMessages = null;
            for (Klient klientsOldKlient : klientsOld) {
                if (!klientsNew.contains(klientsOldKlient)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Klient " + klientsOldKlient + " since its adres field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Set<Klient> attachedKlientsNew = new HashSet<Klient>();
            for (Klient klientsNewKlientToAttach : klientsNew) {
                klientsNewKlientToAttach = em.getReference(Klient.class, klientsNewKlientToAttach.getIdKlienta());
                attachedKlientsNew.add(klientsNewKlientToAttach);
            }
            klientsNew = attachedKlientsNew;
            adres.setKlients(klientsNew);
            adres = em.merge(adres);
            for (Klient klientsNewKlient : klientsNew) {
                if (!klientsOld.contains(klientsNewKlient)) {
                    Adres oldAdresOfKlientsNewKlient = klientsNewKlient.getAdres();
                    klientsNewKlient.setAdres(adres);
                    klientsNewKlient = em.merge(klientsNewKlient);
                    if (oldAdresOfKlientsNewKlient != null && !oldAdresOfKlientsNewKlient.equals(adres)) {
                        oldAdresOfKlientsNewKlient.getKlients().remove(klientsNewKlient);
                        oldAdresOfKlientsNewKlient = em.merge(oldAdresOfKlientsNewKlient);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = adres.getIdAdresu();
                if (findAdres(id) == null) {
                    throw new NonexistentEntityException("The adres with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        em.getTransaction().begin();
        Adres adres;
        try {
            adres = em.getReference(Adres.class, id);
            adres.getIdAdresu();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The adres with id " + id + " no longer exists.", enfe);
        }
        List<String> illegalOrphanMessages = null;
        Set<Klient> klientsOrphanCheck = adres.getKlients();
        for (Klient klientsOrphanCheckKlient : klientsOrphanCheck) {
            if (illegalOrphanMessages == null) {
                illegalOrphanMessages = new ArrayList<String>();
            }
            illegalOrphanMessages.add("This Adres (" + adres + ") cannot be destroyed since the Klient " + klientsOrphanCheckKlient + " in its klients field has a non-nullable adres field.");
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        em.remove(adres);
        em.getTransaction().commit();
    }

    public List<Adres> findAdresEntities() {
        return findAdresEntities(true, -1, -1);
    }

    public List<Adres> findAdresEntities(int maxResults, int firstResult) {
        return findAdresEntities(false, maxResults, firstResult);
    }

    private List<Adres> findAdresEntities(boolean all, int maxResults, int firstResult) {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Adres.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Adres findAdres(Long id) {
        return em.find(Adres.class, id);
    }

    public int getAdresCount() {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Adres> rt = cq.from(Adres.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
