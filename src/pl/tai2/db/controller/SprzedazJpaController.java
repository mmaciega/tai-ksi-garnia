package pl.tai2.db.controller;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pl.tai2.db.DbManager;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.model.Koszyk;
import pl.tai2.db.model.Sprzedaz;

/**
 *
 * @author Mateusz Macięga
 */
public class SprzedazJpaController implements Serializable {

    private final EntityManager em;

    public SprzedazJpaController() {
        this.em = DbManager.getInstance().getEntityManager();
    }

    public void create(Sprzedaz sprzedaz) {
        em.getTransaction().begin();
        Koszyk koszyk = sprzedaz.getKoszyk();
        if (koszyk != null) {
            koszyk = em.getReference(Koszyk.class, koszyk.getIdKoszyka());
            sprzedaz.setKoszyk(koszyk);
        }
        em.persist(sprzedaz);
        if (koszyk != null) {
            koszyk.getSprzedazs().add(sprzedaz);
            koszyk = em.merge(koszyk);
        }
        em.getTransaction().commit();
    }

    public void edit(Sprzedaz sprzedaz) throws NonexistentEntityException, Exception {
        try {
            em.getTransaction().begin();
            Sprzedaz persistentSprzedaz = em.find(Sprzedaz.class, sprzedaz.getIdSprzedazy());
            Koszyk koszykOld = persistentSprzedaz.getKoszyk();
            Koszyk koszykNew = sprzedaz.getKoszyk();
            if (koszykNew != null) {
                koszykNew = em.getReference(Koszyk.class, koszykNew.getIdKoszyka());
                sprzedaz.setKoszyk(koszykNew);
            }
            sprzedaz = em.merge(sprzedaz);
            if (koszykOld != null && !koszykOld.equals(koszykNew)) {
                koszykOld.getSprzedazs().remove(sprzedaz);
                koszykOld = em.merge(koszykOld);
            }
            if (koszykNew != null && !koszykNew.equals(koszykOld)) {
                koszykNew.getSprzedazs().add(sprzedaz);
                koszykNew = em.merge(koszykNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = sprzedaz.getIdSprzedazy();
                if (findSprzedaz(id) == null) {
                    throw new NonexistentEntityException("The sprzedaz with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        em.getTransaction().begin();
        Sprzedaz sprzedaz;
        try {
            sprzedaz = em.getReference(Sprzedaz.class, id);
            sprzedaz.getIdSprzedazy();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The sprzedaz with id " + id + " no longer exists.", enfe);
        }
        Koszyk koszyk = sprzedaz.getKoszyk();
        if (koszyk != null) {
            koszyk.getSprzedazs().remove(sprzedaz);
            koszyk = em.merge(koszyk);
        }
        em.remove(sprzedaz);
        em.getTransaction().commit();
    }

    public List<Sprzedaz> findSprzedazEntities() {
        return findSprzedazEntities(true, -1, -1);
    }

    public List<Sprzedaz> findSprzedazEntities(int maxResults, int firstResult) {
        return findSprzedazEntities(false, maxResults, firstResult);
    }

    private List<Sprzedaz> findSprzedazEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Sprzedaz.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Sprzedaz findSprzedaz(Long id) {

        return em.find(Sprzedaz.class, id);
    }

    public int getSprzedazCount() {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Sprzedaz> rt = cq.from(Sprzedaz.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
