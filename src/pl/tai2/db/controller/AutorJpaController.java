package pl.tai2.db.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pl.tai2.db.model.Ksiazka;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import pl.tai2.db.DbManager;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.model.Autor;

/**
 *
 * @author Mateusz Macięga
 */
public class AutorJpaController implements Serializable {

    private EntityManager em;

    public AutorJpaController() {
        this.em = DbManager.getInstance().getEntityManager();
    }

    public void create(Autor autor) {
        if (autor.getKsiazkaList() == null) {
            autor.setKsiazkaList(new ArrayList<>());
        }
        em.getTransaction().begin();
        List<Ksiazka> attachedKsiazkaList = new ArrayList<Ksiazka>();
        for (Ksiazka ksiazkaListKsiazkaToAttach : autor.getKsiazkaList()) {
            ksiazkaListKsiazkaToAttach = em.getReference(ksiazkaListKsiazkaToAttach.getClass(), ksiazkaListKsiazkaToAttach.getIdKsiazki());
            attachedKsiazkaList.add(ksiazkaListKsiazkaToAttach);
        }
        autor.setKsiazkaList(attachedKsiazkaList);
        em.persist(autor);
        for (Ksiazka ksiazkaListKsiazka : autor.getKsiazkaList()) {
            ksiazkaListKsiazka.getAutorList().add(autor);
            ksiazkaListKsiazka = em.merge(ksiazkaListKsiazka);
        }
        em.getTransaction().commit();

    }

    public void edit(Autor autor) throws NonexistentEntityException, Exception {
        try {
            em.getTransaction().begin();
            Autor persistentAutor = em.find(Autor.class, autor.getIdAutora());
            List<Ksiazka> ksiazkaListOld = persistentAutor.getKsiazkaList();
            List<Ksiazka> ksiazkaListNew = autor.getKsiazkaList();
            List<Ksiazka> attachedKsiazkaListNew = new ArrayList<Ksiazka>();
            for (Ksiazka ksiazkaListNewKsiazkaToAttach : ksiazkaListNew) {
                ksiazkaListNewKsiazkaToAttach = em.getReference(ksiazkaListNewKsiazkaToAttach.getClass(), ksiazkaListNewKsiazkaToAttach.getIdKsiazki());
                attachedKsiazkaListNew.add(ksiazkaListNewKsiazkaToAttach);
            }
            ksiazkaListNew = attachedKsiazkaListNew;
            autor.setKsiazkaList(ksiazkaListNew);
            autor = em.merge(autor);
            for (Ksiazka ksiazkaListOldKsiazka : ksiazkaListOld) {
                if (!ksiazkaListNew.contains(ksiazkaListOldKsiazka)) {
                    ksiazkaListOldKsiazka.getAutorList().remove(autor);
                    ksiazkaListOldKsiazka = em.merge(ksiazkaListOldKsiazka);
                }
            }
            for (Ksiazka ksiazkaListNewKsiazka : ksiazkaListNew) {
                if (!ksiazkaListOld.contains(ksiazkaListNewKsiazka)) {
                    ksiazkaListNewKsiazka.getAutorList().add(autor);
                    ksiazkaListNewKsiazka = em.merge(ksiazkaListNewKsiazka);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = autor.getIdAutora();
                if (findAutor(id) == null) {
                    throw new NonexistentEntityException("The autor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        em.getTransaction().begin();
        Autor autor;
        try {
            autor = em.getReference(Autor.class, id);
            autor.getIdAutora();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The autor with id " + id + " no longer exists.", enfe);
        }
        List<Ksiazka> ksiazkaList = autor.getKsiazkaList();
        for (Ksiazka ksiazkaListKsiazka : ksiazkaList) {
            ksiazkaListKsiazka.getAutorList().remove(autor);
            ksiazkaListKsiazka = em.merge(ksiazkaListKsiazka);
        }
        em.remove(autor);
        em.getTransaction().commit();
    }

    public List<Autor> findAutorEntities() {
        return findAutorEntities(true, -1, -1);
    }

    public List<Autor> findAutorEntities(int maxResults, int firstResult) {
        return findAutorEntities(false, maxResults, firstResult);
    }

    private List<Autor> findAutorEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Autor.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Autor findAutor(Long id) {
        return em.find(Autor.class, id);
    }

    public int getAutorCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Autor> rt = cq.from(Autor.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
