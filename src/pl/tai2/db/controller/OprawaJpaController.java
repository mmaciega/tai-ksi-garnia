package pl.tai2.db.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pl.tai2.db.model.Ksiazka;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import pl.tai2.db.DbManager;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.model.Oprawa;

/**
 *
 * @author Mateusz Macięga
 */
public class OprawaJpaController implements Serializable {

    private final EntityManager em;

    public OprawaJpaController() {
        this.em = DbManager.getInstance().getEntityManager();
    }

    public void create(Oprawa oprawa) {
        if (oprawa.getKsiazkaList() == null) {
            oprawa.setKsiazkaList(new ArrayList<>());
        }
        em.getTransaction().begin();
        List<Ksiazka> attachedKsiazkaList = new ArrayList<Ksiazka>();
        for (Ksiazka ksiazkaListKsiazkaToAttach : oprawa.getKsiazkaList()) {
            ksiazkaListKsiazkaToAttach = em.getReference(Ksiazka.class, ksiazkaListKsiazkaToAttach.getIdKsiazki());
            attachedKsiazkaList.add(ksiazkaListKsiazkaToAttach);
        }
        oprawa.setKsiazkaList(attachedKsiazkaList);
        em.persist(oprawa);
        for (Ksiazka ksiazkaListKsiazka : oprawa.getKsiazkaList()) {
            Oprawa oldOprawaOfKsiazkaListKsiazka = ksiazkaListKsiazka.getOprawa();
            ksiazkaListKsiazka.setOprawa(oprawa);
            ksiazkaListKsiazka = em.merge(ksiazkaListKsiazka);
            if (oldOprawaOfKsiazkaListKsiazka != null) {
                oldOprawaOfKsiazkaListKsiazka.getKsiazkaList().remove(ksiazkaListKsiazka);
                oldOprawaOfKsiazkaListKsiazka = em.merge(oldOprawaOfKsiazkaListKsiazka);
            }
        }
        em.getTransaction().commit();
    }

    public void edit(Oprawa oprawa) throws NonexistentEntityException, Exception {
        try {
            em.getTransaction().begin();
            Oprawa persistentOprawa = em.find(Oprawa.class, oprawa.getIdOprawy());
            List<Ksiazka> ksiazkaListOld = persistentOprawa.getKsiazkaList();
            List<Ksiazka> ksiazkaListNew = oprawa.getKsiazkaList();
            List<Ksiazka> attachedKsiazkaListNew = new ArrayList<Ksiazka>();
            for (Ksiazka ksiazkaListNewKsiazkaToAttach : ksiazkaListNew) {
                ksiazkaListNewKsiazkaToAttach = em.getReference(Ksiazka.class, ksiazkaListNewKsiazkaToAttach.getIdKsiazki());
                attachedKsiazkaListNew.add(ksiazkaListNewKsiazkaToAttach);
            }
            ksiazkaListNew = attachedKsiazkaListNew;
            oprawa.setKsiazkaList(ksiazkaListNew);
            oprawa = em.merge(oprawa);
            for (Ksiazka ksiazkaListOldKsiazka : ksiazkaListOld) {
                if (!ksiazkaListNew.contains(ksiazkaListOldKsiazka)) {
                    ksiazkaListOldKsiazka.setOprawa(null);
                    ksiazkaListOldKsiazka = em.merge(ksiazkaListOldKsiazka);
                }
            }
            for (Ksiazka ksiazkaListNewKsiazka : ksiazkaListNew) {
                if (!ksiazkaListOld.contains(ksiazkaListNewKsiazka)) {
                    Oprawa oldOprawaOfKsiazkaListNewKsiazka = ksiazkaListNewKsiazka.getOprawa();
                    ksiazkaListNewKsiazka.setOprawa(oprawa);
                    ksiazkaListNewKsiazka = em.merge(ksiazkaListNewKsiazka);
                    if (oldOprawaOfKsiazkaListNewKsiazka != null && !oldOprawaOfKsiazkaListNewKsiazka.equals(oprawa)) {
                        oldOprawaOfKsiazkaListNewKsiazka.getKsiazkaList().remove(ksiazkaListNewKsiazka);
                        oldOprawaOfKsiazkaListNewKsiazka = em.merge(oldOprawaOfKsiazkaListNewKsiazka);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = oprawa.getIdOprawy();
                if (findOprawa(id) == null) {
                    throw new NonexistentEntityException("The oprawa with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        em.getTransaction().begin();
        Oprawa oprawa;
        try {
            oprawa = em.getReference(Oprawa.class, id);
            oprawa.getIdOprawy();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The oprawa with id " + id + " no longer exists.", enfe);
        }
        List<Ksiazka> ksiazkaList = oprawa.getKsiazkaList();
        for (Ksiazka ksiazkaListKsiazka : ksiazkaList) {
            ksiazkaListKsiazka.setOprawa(null);
            ksiazkaListKsiazka = em.merge(ksiazkaListKsiazka);
        }
        em.remove(oprawa);
        em.getTransaction().commit();
    }

    public List<Oprawa> findOprawaEntities() {
        return findOprawaEntities(true, -1, -1);
    }

    public List<Oprawa> findOprawaEntities(int maxResults, int firstResult) {
        return findOprawaEntities(false, maxResults, firstResult);
    }

    private List<Oprawa> findOprawaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Oprawa.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Oprawa findOprawa(Long id) {
        return em.find(Oprawa.class, id);
    }

    public int getOprawaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Oprawa> rt = cq.from(Oprawa.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
