package pl.tai2.db.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pl.tai2.db.model.Oprawa;
import pl.tai2.db.model.Wydawnictwo;
import pl.tai2.db.model.Autor;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import pl.tai2.db.DbManager;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.model.Ksiazka;
import pl.tai2.db.model.Podkategoria;

/**
 *
 * @author Mateusz Macięga
 */
public class KsiazkaJpaController implements Serializable {

    private final EntityManager em;

    public KsiazkaJpaController() {
        this.em = DbManager.getInstance().getEntityManager();
    }

    public void create(Ksiazka ksiazka) {
        if (ksiazka.getAutorList() == null) {
            ksiazka.setAutorList(new ArrayList<>());
        }
        if (ksiazka.getPodkategoriaList() == null) {
            ksiazka.setPodkategoriaList(new ArrayList<>());
        }

        em.getTransaction().begin();
        Oprawa oprawa = ksiazka.getOprawa();
        if (oprawa != null) {
            oprawa = em.getReference(Oprawa.class, oprawa.getIdOprawy());
            ksiazka.setOprawa(oprawa);
        }
        Wydawnictwo wydawnictwo = ksiazka.getWydawnictwo();
        if (wydawnictwo != null) {
            wydawnictwo = em.getReference(Wydawnictwo.class, wydawnictwo.getIdWydawnictwa());
            ksiazka.setWydawnictwo(wydawnictwo);
        }
        List<Autor> attachedAutorList = new ArrayList<Autor>();
        for (Autor autorListAutorToAttach : ksiazka.getAutorList()) {
            autorListAutorToAttach = em.getReference(Autor.class, autorListAutorToAttach.getIdAutora());
            attachedAutorList.add(autorListAutorToAttach);
        }
        ksiazka.setAutorList(attachedAutorList);
        List<Podkategoria> attachedPodkategoriaList = new ArrayList<Podkategoria>();
        for (Podkategoria podkategoriaListPodkategoriaToAttach : ksiazka.getPodkategoriaList()) {
            podkategoriaListPodkategoriaToAttach = em.getReference(Podkategoria.class, podkategoriaListPodkategoriaToAttach.getIdPodkategorii());
            attachedPodkategoriaList.add(podkategoriaListPodkategoriaToAttach);
        }
        ksiazka.setPodkategoriaList(attachedPodkategoriaList);
        em.persist(ksiazka);
        if (oprawa != null) {
            oprawa.getKsiazkaList().add(ksiazka);
            oprawa = em.merge(oprawa);
        }
        if (wydawnictwo != null) {
            wydawnictwo.getKsiazkaList().add(ksiazka);
            wydawnictwo = em.merge(wydawnictwo);
        }
        for (Autor autorListAutor : ksiazka.getAutorList()) {
            autorListAutor.getKsiazkaList().add(ksiazka);
            autorListAutor = em.merge(autorListAutor);
        }
        for (Podkategoria podkategoriaListPodkategoria : ksiazka.getPodkategoriaList()) {
            podkategoriaListPodkategoria.getKsiazkaList().add(ksiazka);
            podkategoriaListPodkategoria = em.merge(podkategoriaListPodkategoria);
        }

        em.getTransaction().commit();
    }

    public void edit(Ksiazka ksiazka) throws NonexistentEntityException, Exception {
        try {
            em.getTransaction().begin();
            Ksiazka persistentKsiazka = em.find(Ksiazka.class, ksiazka.getIdKsiazki());
            Oprawa oprawaOld = persistentKsiazka.getOprawa();
            Oprawa oprawaNew = ksiazka.getOprawa();
            Wydawnictwo wydawnictwoOld = persistentKsiazka.getWydawnictwo();
            Wydawnictwo wydawnictwoNew = ksiazka.getWydawnictwo();
            List<Autor> autorListOld = persistentKsiazka.getAutorList();
            List<Autor> autorListNew = ksiazka.getAutorList();
            List<Podkategoria> podkategoriaListOld = persistentKsiazka.getPodkategoriaList();
            List<Podkategoria> podkategoriaListNew = ksiazka.getPodkategoriaList();
            if (oprawaNew != null) {
                oprawaNew = em.getReference(Oprawa.class, oprawaNew.getIdOprawy());
                ksiazka.setOprawa(oprawaNew);
            }
            if (wydawnictwoNew != null) {
                wydawnictwoNew = em.getReference(Wydawnictwo.class, wydawnictwoNew.getIdWydawnictwa());
                ksiazka.setWydawnictwo(wydawnictwoNew);
            }
            List<Autor> attachedAutorListNew = new ArrayList<Autor>();
            for (Autor autorListNewAutorToAttach : autorListNew) {
                autorListNewAutorToAttach = em.getReference(Autor.class, autorListNewAutorToAttach.getIdAutora());
                attachedAutorListNew.add(autorListNewAutorToAttach);
            }
            autorListNew = attachedAutorListNew;
            ksiazka.setAutorList(autorListNew);
            List<Podkategoria> attachedPodkategoriaListNew = new ArrayList<Podkategoria>();
            for (Podkategoria podkategoriaListNewPodkategoriaToAttach : podkategoriaListNew) {
                podkategoriaListNewPodkategoriaToAttach = em.getReference(Podkategoria.class, podkategoriaListNewPodkategoriaToAttach.getIdPodkategorii());
                attachedPodkategoriaListNew.add(podkategoriaListNewPodkategoriaToAttach);
            }
            podkategoriaListNew = attachedPodkategoriaListNew;
            ksiazka.setPodkategoriaList(podkategoriaListNew);
            ksiazka = em.merge(ksiazka);
            if (oprawaOld != null && !oprawaOld.equals(oprawaNew)) {
                oprawaOld.getKsiazkaList().remove(ksiazka);
                oprawaOld = em.merge(oprawaOld);
            }
            if (oprawaNew != null && !oprawaNew.equals(oprawaOld)) {
                oprawaNew.getKsiazkaList().add(ksiazka);
                oprawaNew = em.merge(oprawaNew);
            }
            if (wydawnictwoOld != null && !wydawnictwoOld.equals(wydawnictwoNew)) {
                wydawnictwoOld.getKsiazkaList().remove(ksiazka);
                wydawnictwoOld = em.merge(wydawnictwoOld);
            }
            if (wydawnictwoNew != null && !wydawnictwoNew.equals(wydawnictwoOld)) {
                wydawnictwoNew.getKsiazkaList().add(ksiazka);
                wydawnictwoNew = em.merge(wydawnictwoNew);
            }
            for (Autor autorListOldAutor : autorListOld) {
                if (!autorListNew.contains(autorListOldAutor)) {
                    autorListOldAutor.getKsiazkaList().remove(ksiazka);
                    autorListOldAutor = em.merge(autorListOldAutor);
                }
            }
            for (Autor autorListNewAutor : autorListNew) {
                if (!autorListOld.contains(autorListNewAutor)) {
                    autorListNewAutor.getKsiazkaList().add(ksiazka);
                    autorListNewAutor = em.merge(autorListNewAutor);
                }
            }
            for (Podkategoria podkategoriaListOldPodkategoria : podkategoriaListOld) {
                if (!podkategoriaListNew.contains(podkategoriaListOldPodkategoria)) {
                    podkategoriaListOldPodkategoria.getKsiazkaList().remove(ksiazka);
                    podkategoriaListOldPodkategoria = em.merge(podkategoriaListOldPodkategoria);
                }
            }
            for (Podkategoria podkategoriaListNewPodkategoria : podkategoriaListNew) {
                if (!podkategoriaListOld.contains(podkategoriaListNewPodkategoria)) {
                    podkategoriaListNewPodkategoria.getKsiazkaList().add(ksiazka);
                    podkategoriaListNewPodkategoria = em.merge(podkategoriaListNewPodkategoria);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = ksiazka.getIdKsiazki();
                if (findKsiazka(id) == null) {
                    throw new NonexistentEntityException("The ksiazka with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        em.getTransaction().begin();
        Ksiazka ksiazka;
        try {
            ksiazka = em.getReference(Ksiazka.class, id);
            ksiazka.getIdKsiazki();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The ksiazka with id " + id + " no longer exists.", enfe);
        }
        Oprawa oprawa = ksiazka.getOprawa();
        if (oprawa != null) {
            oprawa.getKsiazkaList().remove(ksiazka);
            oprawa = em.merge(oprawa);
        }
        Wydawnictwo wydawnictwo = ksiazka.getWydawnictwo();
        if (wydawnictwo != null) {
            wydawnictwo.getKsiazkaList().remove(ksiazka);
            wydawnictwo = em.merge(wydawnictwo);
        }
        List<Autor> autorList = ksiazka.getAutorList();
        for (Autor autorListAutor : autorList) {
            autorListAutor.getKsiazkaList().remove(ksiazka);
            autorListAutor = em.merge(autorListAutor);
        }
        List<Podkategoria> podkategoriaList = ksiazka.getPodkategoriaList();
        for (Podkategoria podkategoriaListPodkategoria : podkategoriaList) {
            podkategoriaListPodkategoria.getKsiazkaList().remove(ksiazka);
            podkategoriaListPodkategoria = em.merge(podkategoriaListPodkategoria);
        }
        em.remove(ksiazka);
        em.getTransaction().commit();
    }

    public List<Ksiazka> findKsiazkaEntities() {
        return findKsiazkaEntities(true, -1, -1);
    }

    public List<Ksiazka> findKsiazkaEntities(int maxResults, int firstResult) {
        return findKsiazkaEntities(false, maxResults, firstResult);
    }

    private List<Ksiazka> findKsiazkaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Ksiazka.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Ksiazka findKsiazka(Long id) {
        return em.find(Ksiazka.class, id);
    }

    public int getKsiazkaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Ksiazka> rt = cq.from(Ksiazka.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
