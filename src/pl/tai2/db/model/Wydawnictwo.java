package pl.tai2.db.model;

import java.util.List;
import java.util.Set;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "WYDAWNICTWO", schema = "PUBLIC", catalog = "TAI2", uniqueConstraints = @UniqueConstraint(columnNames = "NAZWA_WYDAWNICTWA"))
public class Wydawnictwo implements java.io.Serializable {
	private static final long serialVersionUID = -8524514423303594866L;
	private LongProperty idWydawnictwa = new SimpleLongProperty();
	private StringProperty nazwaWydawnictwa = new SimpleStringProperty();
	private ObservableList<Ksiazka> ksiazkaList = FXCollections.observableArrayList();

	public Wydawnictwo() {
	}

	public Wydawnictwo(String nazwaWydawnictwa) {
		this.nazwaWydawnictwa = new SimpleStringProperty(nazwaWydawnictwa);
	}

	public Wydawnictwo(String nazwaWydawnictwa, Set<Ksiazka> ksiazkaList) {
		this.nazwaWydawnictwa = new SimpleStringProperty(nazwaWydawnictwa);
		this.ksiazkaList = FXCollections.observableArrayList(ksiazkaList);
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_WYDAWNICTWA", unique = true, nullable = false)
	public Long getIdWydawnictwa() {
		return this.idWydawnictwa.get();
	}

	public void setIdWydawnictwa(Long idWydawnictwa) {
		this.idWydawnictwa.set(idWydawnictwa);
	}
        
	public LongProperty idWydawnictwaProperty() {
		return idWydawnictwa;
	}

	@Column(name = "NAZWA_WYDAWNICTWA", unique = true, nullable = false, length = 80)
	public String getNazwaWydawnictwa() {
		return this.nazwaWydawnictwa.get();
	}

	public void setNazwaWydawnictwa(String nazwaWydawnictwa) {
		this.nazwaWydawnictwa.set(nazwaWydawnictwa);
	}
        
	public StringProperty nazwaWydawnictwaProperty() {
		return nazwaWydawnictwa;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "wydawnictwo")
	public List<Ksiazka> getKsiazkaList() {
		return this.ksiazkaList;
	}

	public void setKsiazkaList(List<Ksiazka> ksiazkaList) {
		this.ksiazkaList = FXCollections.observableArrayList(ksiazkaList);
	}
        
	public ObservableList<Ksiazka> ksiazkaListProperty() {
		return this.ksiazkaList;
	}

}
