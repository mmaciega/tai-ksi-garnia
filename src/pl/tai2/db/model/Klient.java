package pl.tai2.db.model;

import java.util.HashSet;
import java.util.Set;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "KLIENT", schema = "PUBLIC", catalog = "TAI2", uniqueConstraints = @UniqueConstraint(columnNames = "EMAIL"))
public class Klient implements java.io.Serializable {
	private static final long serialVersionUID = 4141482405327293040L;
	private LongProperty idKlienta = new SimpleLongProperty();
	private ObjectProperty<Adres> adres = new SimpleObjectProperty();
	private StringProperty imieKlienta = new SimpleStringProperty();
	private StringProperty nazwiskoKlienta = new SimpleStringProperty();
	private StringProperty email = new SimpleStringProperty();
	private IntegerProperty nrTelefonu;
	private Set<Koszyk> koszyks = new HashSet<Koszyk>(0);

	public Klient() {
	}

	public Klient(Adres adres, String imieKlienta, String nazwiskoKlienta,
			String email) {
		this.adres = new SimpleObjectProperty(adres);
		this.imieKlienta = new SimpleStringProperty(imieKlienta);
		this.nazwiskoKlienta = new SimpleStringProperty(nazwiskoKlienta);
		this.email = new SimpleStringProperty(email);
	}

	public Klient(Adres adres, String imieKlienta, String nazwiskoKlienta,
			String email, Integer nrTelefonu, char plec,
			Set<Koszyk> koszyks) {
		this.adres = new SimpleObjectProperty(adres);
		this.imieKlienta = new SimpleStringProperty(imieKlienta);
		this.nazwiskoKlienta = new SimpleStringProperty(nazwiskoKlienta);
		this.email = new SimpleStringProperty(email);
		this.nrTelefonu = new SimpleIntegerProperty(nrTelefonu);
		this.koszyks = koszyks;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_KLIENTA", unique = true, nullable = false)
	public Long getIdKlienta() {
		return this.idKlienta.get();
	}

	public void setIdKlienta(Long idKlienta) {
		this.idKlienta.set(idKlienta);
	}
        
	public LongProperty idKlientaProperty() {
		return idKlienta;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ADRESU", nullable = false)
	public Adres getAdres() {
		return this.adres.get();
	}

	public void setAdres(Adres adres) {
		this.adres.set(adres);
	}
        
	public ObjectProperty<Adres> adresProperty() {
		return adres;
	}

	@Column(name = "IMIE_KLIENTA", nullable = false, length = 50)
	public String getImieKlienta() {
		return this.imieKlienta.get();
	}

	public void setImieKlienta(String imieKlienta) {
		this.imieKlienta.set(imieKlienta);
	}
        
	public StringProperty imieKlientaProperty() {
		return imieKlienta;
	}


	@Column(name = "NAZWISKO_KLIENTA", nullable = false, length = 75)
	public String getNazwiskoKlienta() {
		return this.nazwiskoKlienta.get();
	}

	public void setNazwiskoKlienta(String nazwiskoKlienta) {
		this.nazwiskoKlienta.set(nazwiskoKlienta);
	}
        
	public StringProperty nazwiskoKlientaProperty() {
		return nazwiskoKlienta;
	}

	@Column(name = "EMAIL", unique = true, nullable = false, length = 90)
	public String getEmail() {
		return this.email.get();
	}

	public void setEmail(String email) {
		this.email.set(email);
	}
        
	public StringProperty emailProperty() {
		return email;
	}

	@Column(name = "NR_TELEFONU", precision = 9, scale = 0)
	public Integer getNrTelefonu() {
            if (nrTelefonu != null)
                return this.nrTelefonu.get();

            return null;
	}

	public void setNrTelefonu(Integer nrTelefonu) {
            if (nrTelefonu != null) {

                if  (this.nrTelefonu == null) 
                    this.nrTelefonu = new SimpleIntegerProperty();

                this.nrTelefonu.set(nrTelefonu);
            }
	}
        
	public IntegerProperty nrTelefonuProperty() {
		return nrTelefonu;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "klient")
	public Set<Koszyk> getKoszyks() {
		return this.koszyks;
	}

	public void setKoszyks(Set<Koszyk> koszyks) {
		this.koszyks = koszyks;
	}

}
