package pl.tai2.db.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ProduktWKoszykuId implements java.io.Serializable {
	private static final long serialVersionUID = -6654449403151326888L;
	private long idKoszyka;
	private long idKsiazki;

	public ProduktWKoszykuId() {
	}

	public ProduktWKoszykuId(long idKoszyka, long idKsiazki) {
		this.idKoszyka = idKoszyka;
		this.idKsiazki = idKsiazki;
	}

	@Column(name = "ID_KOSZYKA", nullable = false)
	public long getIdKoszyka() {
		return this.idKoszyka;
	}

	public void setIdKoszyka(long idKoszyka) {
		this.idKoszyka = idKoszyka;
	}

	@Column(name = "ID_KSIAZKI", nullable = false)
	public long getIdKsiazki() {
		return this.idKsiazki;
	}

	public void setIdKsiazki(long idKsiazki) {
		this.idKsiazki = idKsiazki;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ProduktWKoszykuId))
			return false;
		ProduktWKoszykuId castOther = (ProduktWKoszykuId) other;

		return (this.getIdKoszyka() == castOther.getIdKoszyka())
				&& (this.getIdKsiazki() == castOther.getIdKsiazki());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (int) this.getIdKoszyka();
		result = 37 * result + (int) this.getIdKsiazki();
		return result;
	}

}
