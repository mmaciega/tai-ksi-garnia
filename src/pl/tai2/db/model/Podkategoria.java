package pl.tai2.db.model;

import java.util.List;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "PODKATEGORIA", schema = "PUBLIC", catalog = "TAI2", uniqueConstraints = @UniqueConstraint(columnNames = "NAZWA_PODKATEGORII"))
public class Podkategoria implements java.io.Serializable {
	private static final long serialVersionUID = -8975331464427440478L;
	private LongProperty idPodkategorii = new SimpleLongProperty();
	private ObjectProperty<Kategoria> kategoria = new SimpleObjectProperty<>();
	private StringProperty nazwaPodkategorii = new SimpleStringProperty();
	private ObservableList<Ksiazka> ksiazkaList = FXCollections.observableArrayList();

	public Podkategoria() {
	}

	public Podkategoria(Kategoria kategoria, String nazwaPodkategorii) {
		this.kategoria = new SimpleObjectProperty<>(kategoria);
		this.nazwaPodkategorii = new SimpleStringProperty(nazwaPodkategorii);
	}

	public Podkategoria(Kategoria kategoria, String nazwaPodkategorii,
			List<Ksiazka> ksiazkaList) {
		this.kategoria = new SimpleObjectProperty<>(kategoria);
		this.nazwaPodkategorii = new SimpleStringProperty(nazwaPodkategorii);
		this.ksiazkaList = FXCollections.observableArrayList(ksiazkaList);
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_PODKATEGORII", unique = true, nullable = false)
	public Long getIdPodkategorii() {
		return this.idPodkategorii.get();
	}

	public void setIdPodkategorii(Long idPodkategorii) {
		this.idPodkategorii.set(idPodkategorii);
	}
        
	public LongProperty idPodkategoriiProperty() {
		return idPodkategorii;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_KATEGORII", nullable = false)
	public Kategoria getKategoria() {
		return this.kategoria.get();
	}

	public void setKategoria(Kategoria kategoria) {
		this.kategoria.set(kategoria);
	}
        
	public ObjectProperty<Kategoria> kategoriaProperty() {
		return kategoria;
	}

	@Column(name = "NAZWA_PODKATEGORII", unique = true, nullable = false, length = 75)
	public String getNazwaPodkategorii() {
		return this.nazwaPodkategorii.get();
	}
	
	
	public void setNazwaPodkategorii(String nazwaPodkategorii) {
		this.nazwaPodkategorii.set(nazwaPodkategorii);
	}
        
	public StringProperty nazwaPodkategoriiProperty() {
		return nazwaPodkategorii;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "podkategoriaList")
	public List<Ksiazka> getKsiazkaList() {
		return this.ksiazkaList;
	}

	public void setKsiazkaList(List<Ksiazka> ksiazkaList) {
		this.ksiazkaList = FXCollections.observableArrayList(ksiazkaList);
	}

        public ObservableList<Ksiazka> ksiazkaListProperty() {
		return this.ksiazkaList;
	}
}
