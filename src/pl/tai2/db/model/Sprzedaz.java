package pl.tai2.db.model;

import java.util.Date;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SPRZEDAZ", schema = "PUBLIC", catalog = "TAI2")
public class Sprzedaz implements java.io.Serializable {
	private static final long serialVersionUID = -6947160107760566101L;
	private LongProperty idSprzedazy = new SimpleLongProperty();
	private ObjectProperty<Koszyk> koszyk = new SimpleObjectProperty();
	private ObjectProperty<Date> dataSprzedazy = new SimpleObjectProperty<>();

	public Sprzedaz() {
	}

	public Sprzedaz(Koszyk koszyk, Date dataSprzedazy) {
		this.koszyk = new SimpleObjectProperty(koszyk);
		this.dataSprzedazy = new SimpleObjectProperty(dataSprzedazy);
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_SPRZEDAZY", unique = true, nullable = false)
	public Long getIdSprzedazy() {
		return this.idSprzedazy.get();
	}

	public void setIdSprzedazy(Long idSprzedazy) {
		this.idSprzedazy.set(idSprzedazy);
	}
        
	public LongProperty idSprzedazyProperty() {
		return idSprzedazy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_KOSZYKA", nullable = false)
	public Koszyk getKoszyk() {
		return this.koszyk.get();
	}

	public void setKoszyk(Koszyk koszyk) {
		this.koszyk.set(koszyk);
	}
        
	public ObjectProperty<Koszyk> koszykProperty() {
		return koszyk;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATA_SPRZEDAZY", nullable = false, length = 23)
	public Date getDataSprzedazy() {
		return this.dataSprzedazy.get();
	}

	public void setDataSprzedazy(Date dataSprzedazy) {
		this.dataSprzedazy.set(dataSprzedazy);
	}
        
	public ObjectProperty<Date> dataSprzedazyProperty() {
		return dataSprzedazy;
	}

}
