package pl.tai2.db.model;

import java.util.List;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "KATEGORIA", schema = "PUBLIC", catalog = "TAI2", uniqueConstraints = @UniqueConstraint(columnNames = "NAZWA_KATEGORII"))
public class Kategoria implements java.io.Serializable {
    private static final long serialVersionUID = 4091288137373013036L;
    private LongProperty idKategorii = new SimpleLongProperty();
    private StringProperty nazwaKategorii = new SimpleStringProperty();
    private ObservableList<Podkategoria> podkategoriaList = FXCollections.observableArrayList();

    public Kategoria() {
    }

    public Kategoria(String nazwaKategorii) {
        this.nazwaKategorii = new SimpleStringProperty(nazwaKategorii);
    }

    public Kategoria(String nazwaKategorii, List<Podkategoria> podkategoriaList) {
        this.nazwaKategorii = new SimpleStringProperty(nazwaKategorii);
        this.podkategoriaList = FXCollections.observableArrayList(podkategoriaList);
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID_KATEGORII", unique = true, nullable = false)
    public Long getIdKategorii() {
        return this.idKategorii.get();
    }

    public void setIdKategorii(Long idKategorii) {
        this.idKategorii.set(idKategorii);
    }

    public LongProperty idKategoriiProperty() {
        return idKategorii;
    }

    @Column(name = "NAZWA_KATEGORII", unique = true, nullable = false, length = 50)
    public String getNazwaKategorii() {
        return this.nazwaKategorii.get();
    }

    public void setNazwaKategorii(String nazwaKategorii) {
        this.nazwaKategorii.set(nazwaKategorii);
    }

    public StringProperty nazwaKategoriiProperty() {
        return nazwaKategorii;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kategoria")
    public List<Podkategoria> getPodkategoriaList() {
        return this.podkategoriaList;
    }

    public void setPodkategoriaList(List<Podkategoria> podkategoriaList) {
        this.podkategoriaList = FXCollections.observableArrayList(podkategoriaList);
    }

}
