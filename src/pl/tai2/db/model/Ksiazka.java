package pl.tai2.db.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
@Table(name = "KSIAZKA", schema = "PUBLIC", catalog = "TAI2")
@Access(AccessType.PROPERTY)
public class Ksiazka implements java.io.Serializable {

    private static final long serialVersionUID = -6051729570143560036L;
    private LongProperty idKsiazki = new SimpleLongProperty();
    private ObjectProperty<Oprawa> oprawa = new SimpleObjectProperty<>();
    private ObjectProperty<Wydawnictwo> wydawnictwo = new SimpleObjectProperty<>();
    private StringProperty tytul = new SimpleStringProperty();
    private ObjectProperty<Byte> numerWydania = new SimpleObjectProperty<>();
    private IntegerProperty liczbaStron;
    private LongProperty isbn = new SimpleLongProperty();
    private ObjectProperty<BigDecimal> cena = new SimpleObjectProperty<>();
    private IntegerProperty issn = new SimpleIntegerProperty();
    private ObjectProperty<Short> numerTomu = new SimpleObjectProperty<>();
    private ObjectProperty<Date> dataWydania;
    private StringProperty opis;
    private ObservableList<Autor> autorList = FXCollections.observableArrayList();
    private ObservableList<Podkategoria> podkategoriaList = FXCollections.observableArrayList();

    public Ksiazka() {
    }

    public Ksiazka(Wydawnictwo wydawnictwo, String tytul, BigDecimal cena,
                   Date dataWydania) {
        this.wydawnictwo = new SimpleObjectProperty<>(wydawnictwo);
        this.tytul = new SimpleStringProperty(tytul);
        this.cena = new SimpleObjectProperty<>(cena);
        this.dataWydania = new SimpleObjectProperty<>(dataWydania);
    }

    public Ksiazka(Oprawa oprawa, Wydawnictwo wydawnictwo, String tytul,
                   Byte numerWydania, Integer liczbaStron, Long isbn, BigDecimal cena,
                   Integer issn, Short numerTomu, Date dataWydania,
                   String opis, List<Autor> autorList, List<Podkategoria> podkategoriaList) {
        this.oprawa = new SimpleObjectProperty<>(oprawa);
        this.wydawnictwo = new SimpleObjectProperty<>(wydawnictwo);
        this.tytul = new SimpleStringProperty(tytul);
        this.numerWydania = new SimpleObjectProperty<>(numerWydania);
        this.liczbaStron = new SimpleIntegerProperty(liczbaStron);
        this.isbn = new SimpleLongProperty(isbn);
        this.cena = new SimpleObjectProperty<>(cena);
        this.issn = new SimpleIntegerProperty(issn);
        this.numerTomu = new SimpleObjectProperty<>(numerTomu);
        this.dataWydania = new SimpleObjectProperty<>(dataWydania);
        this.opis = new SimpleStringProperty(opis);
        this.autorList = FXCollections.observableArrayList(autorList);
        this.podkategoriaList = FXCollections.observableArrayList(podkategoriaList);
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID_KSIAZKI", unique = true, nullable = false)
    public Long getIdKsiazki() {
        return this.idKsiazki.get();
    }

    public void setIdKsiazki(Long idKsiazki) {
        this.idKsiazki.set(idKsiazki);
    }

    public LongProperty idKsiazkiProperty() {
        return idKsiazki;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "ID_OPRAWY")
    public Oprawa getOprawa() {
        return this.oprawa.get();
    }

    public void setOprawa(Oprawa oprawa) {
        this.oprawa.set(oprawa);
    }

    public ObjectProperty<Oprawa> oprawaProperty() {
        return oprawa;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_WYDAWNICTWA", nullable = false)
    public Wydawnictwo getWydawnictwo() {
        return this.wydawnictwo.get();
    }

    public void setWydawnictwo(Wydawnictwo wydawnictwo) {
        this.wydawnictwo.set(wydawnictwo);
    }

    public ObjectProperty<Wydawnictwo> wydawnictwoProperty() {
        return wydawnictwo;
    }

    @Column(name = "TYTUL", nullable = false, length = 150)
    public String getTytul() {
        return this.tytul.get();
    }

    public void setTytul(String tytul) {
        this.tytul.set(tytul);
    }

    public StringProperty tytulProperty() {
        return tytul;
    }

    @Column(name = "NUMER_WYDANIA", precision = 2, scale = 0)
    public Byte getNumerWydania() {
        return this.numerWydania.get();
    }

    public void setNumerWydania(Byte numerWydania) {
        this.numerWydania.set(numerWydania);
    }

    public ObjectProperty<Byte> numerWydaniaProperty() {
        return numerWydania;
    }

    @Column(name = "LICZBA_STRON", precision = 6, scale = 0)
    public Integer getLiczbaStron() {
        if (liczbaStron != null)
            return this.liczbaStron.get();
        
        return null;
    }

    public void setLiczbaStron(Integer liczbaStron) {
        if (liczbaStron != null) {

            if  (this.liczbaStron == null) 
                this.liczbaStron = new SimpleIntegerProperty();

            this.liczbaStron.set(liczbaStron);
        }
    }

    @Transient
    public IntegerProperty liczbaStronProperty() {
        return liczbaStron;
    }

    @Column(name = "ISBN", precision = 13, scale = 0)
    public Long getIsbn() {
        return this.isbn.get();
    }

    public void setIsbn(Long isbn) {
        this.isbn.setValue(isbn);
    }

    @Transient
    public LongProperty isbnProperty() {
        return isbn;
    }

    @Column(name = "CENA", nullable = false, precision = 6)
    public BigDecimal getCena() {
        return this.cena.get();
    }

    public void setCena(BigDecimal cena) {
        this.cena.set(cena);
    }

    public ObjectProperty<BigDecimal> cenaProperty() {
        return cena;
    }

    @Column(name = "ISSN", precision = 8, scale = 0)
    public Integer getIssn() {
        return this.issn.get();
    }

    public void setIssn(Integer issn) {
        this.issn.setValue(issn);
    }

    @Transient
    public IntegerProperty issnProperty() {
        return issn;
    }

    @Column(name = "NUMER_TOMU", precision = 3, scale = 0)
    public Short getNumerTomu() {
        return this.numerTomu.get();
    }

    public void setNumerTomu(Short numerTomu) {
        this.numerTomu.set(numerTomu);
    }

    public ObjectProperty<Short> numerTomuProperty() {
        return numerTomu;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATA_WYDANIA", length = 8)
    public Date getDataWydania() {
        if(dataWydania != null)
        return this.dataWydania.get();
        
        return null;
    }

    public void setDataWydania(Date dataWydania) {
        if (dataWydania != null) {
            
            if (this.dataWydania == null) {
                this.dataWydania = new SimpleObjectProperty<>();
            }
            
            this.dataWydania.set(dataWydania);
        
        }
    }

    public ObjectProperty<Date> dataWydaniaProperty() {
        return dataWydania;
    }

    @Column(name = "OPIS", length = 2500)
    public String getOpis() {
        if (opis != null)
            return this.opis.get();
        
        return null;
    }

    public void setOpis(String opis) {
        if (opis != null && !opis.equals("")) {

            if  (this.opis == null) 
                this.opis = new SimpleStringProperty();

            this.opis.set(opis);
        }
    }

    public StringProperty opisProperty() {
        return opis;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "AUTOR_KSIAZKI", schema = "PUBLIC", catalog = "TAI2",
            joinColumns = {
                @JoinColumn(name = "ID_KSIAZKI", nullable = false, updatable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = "ID_AUTORA", nullable = false, updatable = false)})
    public List<Autor> getAutorList() {
        return this.autorList;
    }

    public void setAutorList(List<Autor> autorList) {
        this.autorList = FXCollections.observableArrayList(autorList);
    }
    
    public ObservableList<Autor> autorListProperty() {
        return autorList;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "PODKATEGORIE_Z_KSIAZKAMI", schema = "PUBLIC", catalog = "TAI2", joinColumns = {
        @JoinColumn(name = "ID_KSIAZKI", nullable = false, updatable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "ID_PODKATEGORII", nullable = false, updatable = false)})
    public List<Podkategoria> getPodkategoriaList() {
        return this.podkategoriaList;
    }

    public void setPodkategoriaList(List<Podkategoria> podkategoriaList) {
        this.podkategoriaList = FXCollections.observableArrayList(podkategoriaList);
    }
    
    public ObservableList<Podkategoria> podkategoriaListProperty() {
        return this.podkategoriaList;
    }
}
