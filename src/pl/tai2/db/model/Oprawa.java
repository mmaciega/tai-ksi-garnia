package pl.tai2.db.model;

// Generated 2015-01-04 23:22:20 by Hibernate Tools 4.3.1

import java.util.List;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "OPRAWA", schema = "PUBLIC", catalog = "TAI2", uniqueConstraints = @UniqueConstraint(columnNames = "RODZAJ_OPRAWY"))
public class Oprawa implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4147306405818459239L;
	private LongProperty idOprawy = new SimpleLongProperty();
	private StringProperty rodzajOprawy = new SimpleStringProperty();
	private ObservableList<Ksiazka> ksiazkaList = FXCollections.observableArrayList();

	public Oprawa() {
	}

	public Oprawa(String rodzajOprawy) {
		this.rodzajOprawy = new SimpleStringProperty(rodzajOprawy);
	}

	public Oprawa(String rodzajOprawy, List<Ksiazka> ksiazkaList) {
		this.rodzajOprawy = new SimpleStringProperty(rodzajOprawy);
		this.ksiazkaList.setAll(ksiazkaList);
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_OPRAWY", unique = true, nullable = false)
	public Long getIdOprawy() {
		return this.idOprawy.get();
	}

	public void setIdOprawy(Long idOprawy) {
		this.idOprawy.set(idOprawy);
	}
        
	public LongProperty idOprawyProperty() {
		return idOprawy;
	}

	@Column(name = "RODZAJ_OPRAWY", unique = true, nullable = false, length = 50)
	public String getRodzajOprawy() {
		return this.rodzajOprawy.get();
	}

	public void setRodzajOprawy(String rodzajOprawy) {
		this.rodzajOprawy.set(rodzajOprawy);
	}
        
	public StringProperty rodzajOprawyProperty() {
		return rodzajOprawy;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "oprawa")
	public List<Ksiazka> getKsiazkaList() {
		return this.ksiazkaList;
	}

	public void setKsiazkaList(List<Ksiazka> ksiazkaList) {
		this.ksiazkaList = FXCollections.observableArrayList(ksiazkaList);
	}

	public ObservableList<Ksiazka> ksiazkaListProperty() {
		return this.ksiazkaList;
	}
}
