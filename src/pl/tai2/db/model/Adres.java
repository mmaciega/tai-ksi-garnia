package pl.tai2.db.model;

import java.util.HashSet;
import java.util.Set;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "ADRES", schema = "PUBLIC", catalog = "TAI2", uniqueConstraints = @UniqueConstraint(columnNames = {
    "LINIA", "LINIA2", "LINIA3", "MIASTO", "REGION", "KOD_POCZTOWY", "KRAJ"}))
public class Adres implements java.io.Serializable {
    private static final long serialVersionUID = -7326860543820910675L;
    private LongProperty idAdresu = new SimpleLongProperty();
    private StringProperty linia = new SimpleStringProperty();
    private StringProperty linia2 = new SimpleStringProperty();
    private StringProperty linia3 = new SimpleStringProperty();
    private StringProperty miasto = new SimpleStringProperty();
    private StringProperty region = new SimpleStringProperty();
    private StringProperty kodPocztowy = new SimpleStringProperty();
    private StringProperty kraj = new SimpleStringProperty();
    ;
	private Set<Klient> klientList = new HashSet<Klient>(0);

    public Adres() {
    }

    public Adres(String linia, String miasto, String region,
                 String kodPocztowy, String kraj) {
        this.linia = new SimpleStringProperty(linia);
        this.miasto = new SimpleStringProperty(miasto);
        this.region = new SimpleStringProperty(region);
        this.kodPocztowy = new SimpleStringProperty(kodPocztowy);
        this.kraj = new SimpleStringProperty(kraj);
    }

    public Adres(String linia, String linia2, String linia3, String miasto,
                 String region, String kodPocztowy, String kraj, Set<Klient> klients) {
        this.linia = new SimpleStringProperty(linia);
        this.linia2 = new SimpleStringProperty(linia2);
        this.linia3 = new SimpleStringProperty(linia3);
        this.miasto = new SimpleStringProperty(miasto);
        this.region = new SimpleStringProperty(region);
        this.kodPocztowy = new SimpleStringProperty(kodPocztowy);
        this.kraj = new SimpleStringProperty(kraj);
        this.klientList = klients;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID_ADRESU", unique = true, nullable = false)
    public Long getIdAdresu() {
        return this.idAdresu.get();
    }

    public void setIdAdresu(Long idAdresu) {
        this.idAdresu.set(idAdresu);
    }

    public LongProperty idAdresuProperty() {
        return idAdresu;
    }

    @Column(name = "LINIA", nullable = false, length = 50)
    public String getLinia() {
        return this.linia.get();
    }

    public void setLinia(String linia) {
        this.linia.set(linia);
    }

    public StringProperty liniaProperty() {
        return linia;
    }

    @Column(name = "LINIA2", length = 50)
    public String getLinia2() {
        return this.linia2.get();
    }

    public void setLinia2(String linia2) {
        this.linia2.set(linia2);
    }

    public StringProperty linia2Property() {
        return linia2;
    }

    @Column(name = "LINIA3", length = 50)
    public String getLinia3() {
        return this.linia3.get();
    }

    public void setLinia3(String linia3) {
        this.linia3.set(linia3);
    }

    public StringProperty linia3Property() {
        return linia3;
    }

    @Column(name = "MIASTO", nullable = false, length = 100)
    public String getMiasto() {
        return this.miasto.get();
    }

    public void setMiasto(String miasto) {
        this.miasto.set(miasto);
    }

    public StringProperty miastoProperty() {
        return miasto;
    }

    @Column(name = "REGION", nullable = false, length = 50)
    public String getRegion() {
        return this.region.get();
    }

    public void setRegion(String region) {
        this.region.set(region);
    }

    public StringProperty regionProperty() {
        return region;
    }

    @Column(name = "KOD_POCZTOWY", nullable = false, length = 12)
    public String getKodPocztowy() {
        return this.kodPocztowy.get();
    }

    public void setKodPocztowy(String kodPocztowy) {
        this.kodPocztowy.set(kodPocztowy);
    }

    public StringProperty kodPocztowyProperty() {
        return kodPocztowy;
    }

    @Column(name = "KRAJ", nullable = false, length = 50)
    public String getKraj() {
        return this.kraj.get();
    }

    public void setKraj(String kraj) {
        this.kraj.set(kraj);
    }

    public StringProperty krajProperty() {
        return kraj;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "adres")
    public Set<Klient> getKlients() {
        return this.klientList;
    }

    public void setKlients(Set<Klient> klients) {
        this.klientList = klients;
    }

}
