package pl.tai2.db.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "PRODUKT_W_KOSZYKU", schema = "PUBLIC", catalog = "TAI2")
public class ProduktWKoszyku implements java.io.Serializable {
	private static final long serialVersionUID = -5719918000520790954L;
	private ProduktWKoszykuId id;
	private ObjectProperty<Koszyk> koszyk = new SimpleObjectProperty();
	private ObjectProperty<Ksiazka> ksiazka= new SimpleObjectProperty();
	private ObjectProperty<Short> liczbaSztuk = new SimpleObjectProperty();;

	public ProduktWKoszyku() {
	}

	public ProduktWKoszyku(ProduktWKoszykuId id, Koszyk koszyk,
			Ksiazka ksiazka, Short liczbaSztuk) {
		this.id = id;
		this.koszyk = new SimpleObjectProperty(koszyk);
		this.ksiazka = new SimpleObjectProperty(ksiazka);
		this.liczbaSztuk = new SimpleObjectProperty(liczbaSztuk);
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "idKoszyka", column = @Column(name = "ID_KOSZYKA", nullable = false)),
			@AttributeOverride(name = "idKsiazki", column = @Column(name = "ID_KSIAZKI", nullable = false)) })
	public ProduktWKoszykuId getId() {
		return this.id;
	}

	public void setId(ProduktWKoszykuId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_KOSZYKA", nullable = false, insertable = false, updatable = false)
	public Koszyk getKoszyk() {
		return this.koszyk.get();
	}

	public void setKoszyk(Koszyk koszyk) {
		this.koszyk.set(koszyk);
	}
        
	public ObjectProperty<Koszyk> koszykProperty() {
		return koszyk;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_KSIAZKI", nullable = false, insertable = false, updatable = false)
	public Ksiazka getKsiazka() {
		return this.ksiazka.get();
	}

	public void setKsiazka(Ksiazka ksiazka) {
		this.ksiazka.set(ksiazka);
	}
        
	public ObjectProperty<Ksiazka> ksiazkaProperty() {
		return ksiazka;
	}

	@Column(name = "LICZBA_SZTUK", nullable = false, precision = 3, scale = 0)
	public Short getLiczbaSztuk() {
		return this.liczbaSztuk.get();
	}

	public void setLiczbaSztuk(Short liczbaSztuk) {
		this.liczbaSztuk.set(liczbaSztuk);
	}
        
	public ObjectProperty<Short> liczbaSztukProperty() {
		return liczbaSztuk;
	}

}
