package pl.tai2.db.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "KOSZYK", schema = "PUBLIC", catalog = "TAI2")
public class Koszyk implements java.io.Serializable {
	private static final long serialVersionUID = -5230535291624703506L;
	private LongProperty idKoszyka = new SimpleLongProperty();
	private ObjectProperty<Klient> klient = new SimpleObjectProperty();
	private List<ProduktWKoszyku> produktWKoszykus = new ArrayList<>(0);
	private Set<Sprzedaz> sprzedazs = new HashSet<Sprzedaz>(0);

	public Koszyk() {
	}

	public Koszyk(Klient klient) {
		this.klient = new SimpleObjectProperty(klient);
	}

	public Koszyk(Klient klient, List<ProduktWKoszyku> produktWKoszykus,
			Set<Sprzedaz> sprzedazs) {
		this.klient = new SimpleObjectProperty(klient);
		this.produktWKoszykus = produktWKoszykus;
		this.sprzedazs = sprzedazs;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_KOSZYKA", unique = true, nullable = false)
	public Long getIdKoszyka() {
		return this.idKoszyka.get();
	}

	public void setIdKoszyka(Long idKoszyka) {
		this.idKoszyka.set(idKoszyka);
	}
        
	public LongProperty idKoszykaProperty() {
		return idKoszyka;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_KLIENTA", nullable = false)
	public Klient getKlient() {
		return this.klient.get();
	}

	public void setKlient(Klient klient) {
		this.klient.set(klient);
	}
        
	public ObjectProperty<Klient> klientProperty() {
		return klient;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "koszyk", cascade = CascadeType.ALL)
	public List<ProduktWKoszyku> getProduktWKoszykus() {
		return this.produktWKoszykus;
	}

	public void setProduktWKoszykus(List<ProduktWKoszyku> produktWKoszykus) {
		this.produktWKoszykus = produktWKoszykus;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "koszyk")
	public Set<Sprzedaz> getSprzedazs() {
		return this.sprzedazs;
	}

	public void setSprzedazs(Set<Sprzedaz> sprzedazs) {
		this.sprzedazs = sprzedazs;
	}

}
