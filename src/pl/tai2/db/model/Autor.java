package pl.tai2.db.model;

import java.util.Date;
import java.util.List;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "AUTOR", schema = "PUBLIC", catalog = "TAI2", uniqueConstraints = @UniqueConstraint(columnNames = "OPIS_AUTORA"))
public class Autor implements java.io.Serializable {
    private static final long serialVersionUID = 6942859398151593522L;
    private LongProperty idAutora = new SimpleLongProperty();
    private StringProperty imie = new SimpleStringProperty();
    private StringProperty nazwisko = new SimpleStringProperty();
    private ObjectProperty<Date> dataUrodzenia = new SimpleObjectProperty<>();
    private StringProperty miejsceUrodzenia = new SimpleStringProperty();
    private StringProperty opisAutora = new SimpleStringProperty();
    private ObservableList<Ksiazka> ksiazkaList = FXCollections.observableArrayList();

    public Autor() {
    }

    public Autor(String nazwisko) {
        this.nazwisko = new SimpleStringProperty(nazwisko);
    }

    public Autor(String imie, String nazwisko, Date dataUrodzenia,
                 String miejsceUrodzenia, String opisAutora, List<Ksiazka> ksiazkaList) {
        this.imie = new SimpleStringProperty(imie);
        this.nazwisko = new SimpleStringProperty(nazwisko);
        this.dataUrodzenia = new SimpleObjectProperty<>(dataUrodzenia);
        this.miejsceUrodzenia = new SimpleStringProperty(miejsceUrodzenia);
        this.opisAutora = new SimpleStringProperty(opisAutora);
        this.ksiazkaList = FXCollections.observableArrayList(ksiazkaList);
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID_AUTORA", unique = true, nullable = false)
    public Long getIdAutora() {
        return this.idAutora.get();
    }

    public void setIdAutora(Long idAutora) {
        this.idAutora.set(idAutora);
    }

    public LongProperty idAutoraProperty() {
        return idAutora;
    }

    @Column(name = "IMIE", length = 50)
    public String getImie() {
        return this.imie.get();
    }

    public void setImie(String imie) {
        this.imie.set(imie);
    }

    public StringProperty imieProperty() {
        return imie;
    }

    @Column(name = "NAZWISKO", nullable = false, length = 75)
    public String getNazwisko() {
        return this.nazwisko.get();
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko.set(nazwisko);
    }

    public StringProperty nazwiskoProperty() {
        return nazwisko;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DATA_URODZENIA", length = 8)
    public Date getDataUrodzenia() {
        return this.dataUrodzenia.get();
    }

    public void setDataUrodzenia(Date dataUrodzenia) {
        this.dataUrodzenia.set(dataUrodzenia);
    }

    public ObjectProperty<Date> dataUrodzeniaProperty() {
        return dataUrodzenia;
    }

    @Column(name = "MIEJSCE_URODZENIA", length = 100)
    public String getMiejsceUrodzenia() {
        return this.miejsceUrodzenia.get();
    }

    public void setMiejsceUrodzenia(String miejsceUrodzenia) {
        this.miejsceUrodzenia.set(miejsceUrodzenia);
    }

    public StringProperty miejsceUrodzeniaProperty() {
        return miejsceUrodzenia;
    }

    @Column(name = "OPIS_AUTORA", unique = true, length = 1000)
    public String getOpisAutora() {
        return this.opisAutora.get();
    }

    public void setOpisAutora(String opisAutora) {
        this.opisAutora.set(opisAutora);
    }

    public StringProperty opisAutoraProperty() {
        return opisAutora;
    }

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "autorList")
    public List<Ksiazka> getKsiazkaList() {
        return this.ksiazkaList;
    }

    public void setKsiazkaList(List<Ksiazka> ksiazkaList) {
        this.ksiazkaList = FXCollections.observableArrayList(ksiazkaList);
    }

    public ObservableList<Ksiazka> ksiazkaListProperty() {
        return this.ksiazkaList;
    }

}
