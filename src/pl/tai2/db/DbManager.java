package pl.tai2.db;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Mateusz Macięga
 */
public class DbManager {

    private final EntityManagerFactory entityManagerFactory;
    private final EntityManager entityManager;

    private DbManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("TAI2PU");
        entityManager = entityManagerFactory.createEntityManager();
    }

    public static DbManager getInstance() {
        return DbManagerHolder.INSTANCE;
    }

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    private static class DbManagerHolder {

        private static final DbManager INSTANCE = new DbManager();
    }

    public void shutDown() {
        entityManager.close();
        entityManagerFactory.close();
    }

}
