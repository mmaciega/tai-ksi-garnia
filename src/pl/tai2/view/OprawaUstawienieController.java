package pl.tai2.view;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;
import pl.tai2.db.model.Oprawa;

/**
 * FXML Controller class
 *
 * @author Mateusz Macięga
 */
public class OprawaUstawienieController implements Initializable {
    @FXML
    private TextField nazwaOprawyPole;
    
    private Oprawa oprawa;
    private Stage scenaOkna;
    private boolean okKlikniete = false;

    public void setScenaOkna(Stage scenaOkna) {
        this.scenaOkna = scenaOkna;
    }

    public void setOprawa(Oprawa oprawa) {
        this.oprawa = oprawa;
        nazwaOprawyPole.setText(oprawa.getRodzajOprawy());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
    @FXML
    public void obsluzOK() {
        if (czyPoprawneDaneKadry()) {
            oprawa.setRodzajOprawy(nazwaOprawyPole.getText());
            
            okKlikniete = true;
            scenaOkna.close();
        }
    }
    
    private boolean czyPoprawneDaneKadry() {
        if (nazwaOprawyPole.getText() != null) {
            nazwaOprawyPole.setText(nazwaOprawyPole.getText().trim());
        }

        String wiadomoscBledu = "";

        if (nazwaOprawyPole.getText() == null || nazwaOprawyPole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawne nazwa oprawy!\n";
        }

        if (wiadomoscBledu.length() == 0) {
            return true;
        } else {
            Dialogs.create()
                    .title("Błędne wartości pól dla oprawy")
                    .masthead("Proszę poprawić niepoprawne wartości pól")
                    .message(wiadomoscBledu)
                    .showError();
            return false;
        }
    }
    
    @FXML
    public void obsluzAnuluj() {
        scenaOkna.close();
    }

    /**
     * @return the okKlikniete
     */
    public boolean isOkKlikniete() {
        return okKlikniete;
    }
    
}
