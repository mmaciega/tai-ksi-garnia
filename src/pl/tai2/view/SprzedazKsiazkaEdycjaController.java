package pl.tai2.view;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.controlsfx.dialog.Dialogs;
import pl.tai2.db.controller.KsiazkaJpaController;
import pl.tai2.db.model.Ksiazka;
import pl.tai2.db.model.ProduktWKoszyku;

/**
 * FXML Controller class
 *
 * @author Mateusz Macięga
 */
public class SprzedazKsiazkaEdycjaController implements Initializable {
    @FXML
    private ComboBox <Ksiazka> ksiazkaPoleWyboru;
    @FXML
    private TextField autorPole;
    @FXML
    private TextField kategoriaPole;
    @FXML
    private TextField liczbaSztukPole;
    
    private Stage scenaOkna;
    private ProduktWKoszyku produktWKoszyku;
    private boolean okKlikniete = false;

    public void setScenaOkna(Stage scenaOkna) {
        this.scenaOkna = scenaOkna;
    }
    
    public void setProduktWKoszyku(ProduktWKoszyku produktWKoszyku) {
        this.produktWKoszyku = produktWKoszyku;
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ksiazkaPoleWyboru.setCellFactory((comboBox) -> {
            return new ListCell<Ksiazka>() {
                @Override
                protected void updateItem(Ksiazka item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(item.getTytul());
                    }
                }
            };
        });

        ksiazkaPoleWyboru.setConverter(new StringConverter<Ksiazka>() {
            @Override
            public String toString(Ksiazka ksiazka) {
                if (ksiazka == null) {
                    return null;
                } else {
                    return ksiazka.getTytul();
                }
            }

            @Override
            public Ksiazka fromString(String ksiazkaString) {
                return null; // No conversion fromString needed.
            }
        });
        
        ksiazkaPoleWyboru.setOnAction((event) ->  {
            Ksiazka ksiazkaWybrana = ksiazkaPoleWyboru.getSelectionModel().getSelectedItem();
            autorPole.setText(ksiazkaWybrana.getAutorList().stream().map((a) -> a.getImie() + " " + a.getNazwisko()).collect(Collectors.joining(", ")));
            kategoriaPole.setText(ksiazkaWybrana.getPodkategoriaList().stream().map((a) -> a.getKategoria().getNazwaKategorii() + " - " + a.getNazwaPodkategorii()).collect(Collectors.joining(", ")));
        });
    } 
    
    @FXML
    public void obsluzOK() {
        if (czyPoprawneDaneProduktuWKoszyku()) {
            produktWKoszyku.setKsiazka(ksiazkaPoleWyboru.getSelectionModel().getSelectedItem());
            produktWKoszyku.setLiczbaSztuk(Short.parseShort(liczbaSztukPole.getText()));
            
            okKlikniete = true;
            scenaOkna.close();
        }
    }
    
    private boolean czyPoprawneDaneProduktuWKoszyku() {
        if (liczbaSztukPole.getText() != null) {
            liczbaSztukPole.setText(liczbaSztukPole.getText().trim());
        }

        String wiadomoscBledu = "";

        if (ksiazkaPoleWyboru.getSelectionModel().isEmpty()) {
            wiadomoscBledu += "Nie wybrano książki!\n";
        }
        
        if (liczbaSztukPole.getText() == null || liczbaSztukPole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawne liczba sztuk!\n";
        } else {
            try {
                Short wartosc = Short.parseShort(liczbaSztukPole.getText());
                
                if (wartosc <= 0 ) {
                    wiadomoscBledu += "Niepoprawny liczba sztuk (musi być dodatnią liczbą całkowitą)!\n";  
                }
            } catch (NumberFormatException e) {
                wiadomoscBledu += "Niepoprawny liczba sztuk (musi być liczbą całkowitą)!\n"; 
            }
        }
            
            

        if (wiadomoscBledu.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Dialogs.create()
                    .title("Błędne wartości pól dla produktu w koszyku")
                    .masthead("Proszę poprawić niepoprawne wartości pól")
                    .message(wiadomoscBledu)
                    .showError();
            return false;
        }
    }
    
    @FXML
    public void obsluzAnuluj() {
        scenaOkna.close();
    }
    
    public boolean isOkKlikniete() {
        return okKlikniete;
    }

    public void setWybraneKsiazki(ObservableList<ProduktWKoszyku> items) {
        KsiazkaJpaController ksiazkaJpaController = new KsiazkaJpaController();
        List<Ksiazka> wszystieKsiazki = ksiazkaJpaController.findKsiazkaEntities();
        List<Ksiazka> wybraneKsiazki = items.stream().map((pwk) -> pwk.getKsiazka()).collect(Collectors.toList());
        List<Ksiazka> pozostaleKsiazki = wszystieKsiazki.stream().filter(k -> !wybraneKsiazki.contains(k)).collect(Collectors.toList());
        ObservableList<Ksiazka> ksiazkaLista = FXCollections.observableArrayList(pozostaleKsiazki);
        ksiazkaPoleWyboru.setItems(ksiazkaLista);
    }
    
}
