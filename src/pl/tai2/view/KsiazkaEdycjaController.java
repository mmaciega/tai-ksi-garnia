package pl.tai2.view;

import java.math.BigDecimal;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.controlsfx.dialog.Dialogs;
import pl.tai2.db.controller.AutorJpaController;
import pl.tai2.db.controller.OprawaJpaController;
import pl.tai2.db.controller.PodkategoriaJpaController;
import pl.tai2.db.controller.WydawnictwoJpaController;
import pl.tai2.db.model.Autor;
import pl.tai2.db.model.Ksiazka;
import pl.tai2.db.model.Oprawa;
import pl.tai2.db.model.Podkategoria;
import pl.tai2.db.model.Wydawnictwo;

/**
 * FXML Controller class
 *
 * @author Mateusz Macięga
 */
public class KsiazkaEdycjaController implements Initializable {

    @FXML
    private TextField tytulPole;
    @FXML
    private ComboBox<Oprawa> oprawaPoleWyboru;
    @FXML
    private ComboBox<Wydawnictwo> wydawnictwoPoleWyboru;
    @FXML
    private TextField numerWydaniaPole;
    @FXML
    private TextField isbnPole;
    @FXML
    private DatePicker dataWydaniaData;
    @FXML
    private TextField cenaPole;
    @FXML
    private TextArea opisObszar;
    @FXML
    private VBox autorzyObszarEl;
    @FXML
    private Button dodajAutoraPrzycisk;
    @FXML
    private VBox podkategoriaObszarEl;
    @FXML
    private Button dodajKategoriePrzycisk;
    @FXML
    private List<ComboBox<Autor>> autorListaPoleWyboru = new ArrayList<>();
    @FXML
    private List<ComboBox<Podkategoria>> podkategoriaListaPoleWyboru = new ArrayList<>();

    private Ksiazka ksiazka;
    private Stage scenaOkna;
    private boolean okKlikniete = false;

    public void setScenaOkna(Stage scenaOkna) {
        this.scenaOkna = scenaOkna;
    }

    public void setKsiazka(Ksiazka ksiazka) {
        this.ksiazka = ksiazka;

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicjalizacjaAutorPoleWyboru();
        inicjalizacjaPodkategoriaPoleWyboru();

        OprawaJpaController oprawaJpaController = new OprawaJpaController();
        ObservableList<Oprawa> oprawaLista = FXCollections.observableArrayList(oprawaJpaController.findOprawaEntities());
        oprawaPoleWyboru.setItems(oprawaLista);

        WydawnictwoJpaController wydawnictwoJpaController = new WydawnictwoJpaController();
        ObservableList<Wydawnictwo> wydawnictwoLista = FXCollections.observableArrayList(wydawnictwoJpaController.findWydawnictwoEntities());
        wydawnictwoPoleWyboru.setItems(wydawnictwoLista);

        dodajAutoraPrzycisk.setOnAction(event -> {
            HBox autorPasek = new HBox(2);

            ComboBox<Autor> nowyAutor = new ComboBox();
            nowyAutor.setMaxWidth(Double.MAX_VALUE);

            List<Autor> wybraniAutorzy = autorListaPoleWyboru.stream().map(c -> c.getSelectionModel().getSelectedItem()).collect(Collectors.toList());
            List<Autor> pozostaliAutorzy = autorListaPoleWyboru.get(0).getItems().stream().filter(s -> !wybraniAutorzy.contains(s)).collect(Collectors.toList());
            ObservableList<Autor> aut = FXCollections.observableArrayList(pozostaliAutorzy);

            nowyAutor.setItems(aut);
            //<editor-fold defaultstate="collapsed" desc="nowyAutor formatowanie">
            nowyAutor.setCellFactory((comboBox) -> {
                return new ListCell<Autor>() {
                    @Override
                    protected void updateItem(Autor item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText(item.getImie() + " " + item.getNazwisko());
                        }
                    }
                };
            });

            nowyAutor.setConverter(new StringConverter<Autor>() {
                @Override
                public String toString(Autor autor) {
                    if (autor == null) {
                        return null;
                    } else {
                        return autor.getImie() + " " + autor.getNazwisko();
                    }
                }

                @Override
                public Autor fromString(String oprawaString) {
                    return null; // No conversion fromString needed.
                }
            });
            //</editor-fold>

            Button usunPrzycisk = new Button("-");
            usunPrzycisk.setPrefWidth(25);
            usunPrzycisk.setOnAction((e) -> {
                HBox hbox = (HBox) ((Button) e.getSource()).getParent();
                autorzyObszarEl.getChildren().remove(hbox);
                autorListaPoleWyboru.remove((ComboBox<Autor>) hbox.getChildren().get(0));
                scenaOkna.sizeToScene();
            });

            HBox.setHgrow(nowyAutor, Priority.ALWAYS);
            autorPasek.getChildren().addAll(nowyAutor, usunPrzycisk);

            autorzyObszarEl.getChildren().add(autorPasek);
            autorListaPoleWyboru.add(nowyAutor);
            scenaOkna.sizeToScene();
        });

        dodajKategoriePrzycisk.setOnAction(event -> {
            HBox podkategoriaPasek = new HBox(2);

            ComboBox<Podkategoria> nowaPodkategoria = new ComboBox();
            nowaPodkategoria.setMaxWidth(Double.MAX_VALUE);
            List<Podkategoria> wybranePodkategorie = podkategoriaListaPoleWyboru.stream().map(c -> c.getSelectionModel().getSelectedItem()).collect(Collectors.toList());
            List<Podkategoria> pozostalePodkategorie = podkategoriaListaPoleWyboru.get(0).getItems().stream().filter(s -> !wybranePodkategorie.contains(s)).collect(Collectors.toList());
            ObservableList<Podkategoria> pod = FXCollections.observableArrayList(pozostalePodkategorie);
            nowaPodkategoria.setItems(pod);
            //<editor-fold defaultstate="collapsed" desc="nowaPodkategoria formatowanie">
            nowaPodkategoria.setCellFactory((comboBox) -> {
                return new ListCell<Podkategoria>() {
                    @Override
                    protected void updateItem(Podkategoria item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText(item.getNazwaPodkategorii());
                        }
                    }
                };
            });

            nowaPodkategoria.setConverter(new StringConverter<Podkategoria>() {
                @Override
                public String toString(Podkategoria podkategoria) {
                    if (podkategoria == null) {
                        return null;
                    } else {
                        return podkategoria.getNazwaPodkategorii();
                    }
                }

                @Override
                public Podkategoria fromString(String podkategoriaString) {
                    return null; // No conversion fromString needed.
                }
            });
//</editor-fold>

            Button usunPrzycisk = new Button("-");
            usunPrzycisk.setPrefWidth(25);
            usunPrzycisk.setOnAction((e) -> {
                HBox hbox = (HBox) ((Button) e.getSource()).getParent();
                podkategoriaObszarEl.getChildren().remove(((Button) e.getSource()).getParent());
                podkategoriaListaPoleWyboru.remove((ComboBox<Podkategoria>) hbox.getChildren().get(0));
                scenaOkna.sizeToScene();
            });

            HBox.setHgrow(nowaPodkategoria, Priority.ALWAYS);
            podkategoriaPasek.getChildren().addAll(nowaPodkategoria, usunPrzycisk);

            podkategoriaObszarEl.getChildren().add(podkategoriaPasek);
            podkategoriaListaPoleWyboru.add(nowaPodkategoria);
            scenaOkna.sizeToScene();
        });

        oprawaPoleWyboru.setCellFactory((comboBox) -> {
            return new ListCell<Oprawa>() {
                @Override
                protected void updateItem(Oprawa item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(item.getRodzajOprawy());
                    }
                }
            };
        });

        oprawaPoleWyboru.setConverter(new StringConverter<Oprawa>() {
            @Override
            public String toString(Oprawa oprawa) {
                if (oprawa == null) {
                    return null;
                } else {
                    return oprawa.getRodzajOprawy();
                }
            }

            @Override
            public Oprawa fromString(String oprawaString) {
                return null; // No conversion fromString needed.
            }
        });

        wydawnictwoPoleWyboru.setCellFactory((comboBox) -> {
            return new ListCell<Wydawnictwo>() {
                @Override
                protected void updateItem(Wydawnictwo item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(item.getNazwaWydawnictwa());
                    }
                }
            };
        });

        wydawnictwoPoleWyboru.setConverter(new StringConverter<Wydawnictwo>() {
            @Override
            public String toString(Wydawnictwo wydawnictwo) {
                if (wydawnictwo == null) {
                    return null;
                } else {
                    return wydawnictwo.getNazwaWydawnictwa();
                }
            }

            @Override
            public Wydawnictwo fromString(String wydawnictwoString) {
                return null;
            }
        });
    }

    private void inicjalizacjaAutorPoleWyboru() {
        HBox autor = (HBox) autorzyObszarEl.getChildren().get(0);
        ComboBox<Autor> autorPoleWyboru = (ComboBox<Autor>) autor.getChildren().get(0);
        autorPoleWyboru.setCellFactory((comboBox) -> {
            return new ListCell<Autor>() {
                @Override
                protected void updateItem(Autor item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(item.getImie() + " " + item.getNazwisko());
                    }
                }
            };
        });

        autorPoleWyboru.setConverter(new StringConverter<Autor>() {
            @Override
            public String toString(Autor autor) {
                if (autor == null) {
                    return null;
                } else {
                    return autor.getImie() + " " + autor.getNazwisko();
                }
            }

            @Override
            public Autor fromString(String oprawaString) {
                return null;
            }
        });
        autorListaPoleWyboru.add(autorPoleWyboru);

        AutorJpaController autorJpaController = new AutorJpaController();
        ObservableList<Autor> autorLista = FXCollections.observableArrayList(autorJpaController.findAutorEntities());
        autorPoleWyboru.setItems(autorLista);
    }

    private void inicjalizacjaPodkategoriaPoleWyboru() {
        HBox kategoria = (HBox) podkategoriaObszarEl.getChildren().get(0);
        ComboBox<Podkategoria> podkatPoleWyboru = (ComboBox<Podkategoria>) kategoria.getChildren().get(0);
        podkatPoleWyboru.setCellFactory((comboBox) -> {
            return new ListCell<Podkategoria>() {
                @Override
                protected void updateItem(Podkategoria item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(item.getNazwaPodkategorii());
                    }
                }
            };
        });

        podkatPoleWyboru.setConverter(new StringConverter<Podkategoria>() {
            @Override
            public String toString(Podkategoria podkategoria) {
                if (podkategoria == null) {
                    return null;
                } else {
                    return podkategoria.getNazwaPodkategorii();
                }
            }

            @Override
            public Podkategoria fromString(String podkategoriaString) {
                return null;
            }
        });
        podkategoriaListaPoleWyboru.add(podkatPoleWyboru);

        PodkategoriaJpaController podkategoriaJpaController = new PodkategoriaJpaController();
        List<Podkategoria> podkategoriaL = podkategoriaJpaController.findPodkategoriaEntities();
        podkategoriaL.sort((p1, p2) -> p1.getNazwaPodkategorii().compareTo(p2.getNazwaPodkategorii()));
        ObservableList<Podkategoria> podkategoriaLista = FXCollections.observableArrayList(podkategoriaL);
        podkatPoleWyboru.setItems(podkategoriaLista);
    }

    @FXML
    public void obsluzAnuluj() {
        scenaOkna.close();
    }

    @FXML
    public void obsluzOK() {
        if (czyPoprawneDaneKadry()) {
            getKsiazka().setTytul(tytulPole.getText());
            autorListaPoleWyboru.stream().map((a) -> a.getSelectionModel().getSelectedItem()).filter((a) -> a != null).distinct().forEach((a) -> getKsiazka().autorListProperty().add(a));
            podkategoriaListaPoleWyboru.stream().map((p) -> p.getSelectionModel().getSelectedItem()).filter((p) -> p != null).distinct().forEach((p) -> getKsiazka().podkategoriaListProperty().add(p));

            getKsiazka().setWydawnictwo(wydawnictwoPoleWyboru.getSelectionModel().getSelectedItem());
            getKsiazka().setOprawa(oprawaPoleWyboru.getSelectionModel().getSelectedItem());

            if (!numerWydaniaPole.getText().equals("")) {
                getKsiazka().setNumerWydania(Byte.parseByte(numerWydaniaPole.getText()));
            }
            getKsiazka().setCena(BigDecimal.valueOf(Double.valueOf(cenaPole.getText())));

            if (dataWydaniaData.getValue() != null) {
                LocalDate ld = dataWydaniaData.getValue();
                Instant instant = ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
                getKsiazka().setDataWydania(Date.from(instant));
            }

            getKsiazka().setOpis(opisObszar.getText());

            okKlikniete = true;
            scenaOkna.close();
        }
    }

    private boolean czyPoprawneDaneKadry() {
        if (tytulPole.getText() != null) {
            tytulPole.setText(tytulPole.getText().trim());
        }

        if (numerWydaniaPole.getText() != null) {
            numerWydaniaPole.setText(numerWydaniaPole.getText().trim());
        }

        if (cenaPole.getText() != null) {
            cenaPole.setText(cenaPole.getText().trim());
        }

        if (opisObszar.getText() != null) {
            opisObszar.setText(opisObszar.getText().trim());
        }

        String wiadomoscBledu = "";

        if (tytulPole.getText() == null || tytulPole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawny tytuł!\n";
        }

        long liczbaAutorow = autorListaPoleWyboru.stream().map(c -> c.getSelectionModel().getSelectedItem()).filter((a) -> a != null).count();
        if (liczbaAutorow <= 0) {
            wiadomoscBledu += "Nie wybrano żadnego autora!\n";
        }

        if (wydawnictwoPoleWyboru.getSelectionModel().isEmpty()) {
            wiadomoscBledu += "Nie wybrano wydawnictwa!\n";
        }
        
        if (numerWydaniaPole.getText() != null) {
            try {
                Integer wartosc = Integer.parseInt(numerWydaniaPole.getText());
                
                if (wartosc <= 0 ) {
                    wiadomoscBledu += "Niepoprawny numer wydania (musi być dodatnią liczbą całkowitą)!\n";  
                }
            } catch (NumberFormatException e) {
                wiadomoscBledu += "Niepoprawny numer wydania (musi być liczbą całkowitą)!\n"; 
            }
        }

        long liczbaPodkategorii = podkategoriaListaPoleWyboru.stream().map(c -> c.getSelectionModel().getSelectedItem()).filter((a) -> a != null).count();
        if (liczbaPodkategorii <= 0) {
            wiadomoscBledu += "Nie wybrano żadnej kategorii!\n";
        }

        if (cenaPole.getText() == null || cenaPole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawna cena za książke!\n";
        }

        final LocalDate dzisiejszaData = LocalDate.now();
 
        if (dataWydaniaData.getValue() != null && dataWydaniaData.getValue().isAfter(dzisiejszaData)) {
            wiadomoscBledu += "Niepoprawdna data wydania ksiażki (przyszłość)!\n";
        }

        if (wiadomoscBledu.length() == 0) {
            return true;
        } else {
            Dialogs.create()
                    .title("Błędne wartości pól dla książki")
                    .masthead("Proszę poprawić niepoprawne wartości pól")
                    .message(wiadomoscBledu)
                    .showError();
            return false;
        }
    }

    /**
     * @return the okKlikniete
     */
    public boolean isOkKlikniete() {
        return okKlikniete;
    }

    /**
     * @return the ksiazka
     */
    public Ksiazka getKsiazka() {
        return ksiazka;
    }

}
