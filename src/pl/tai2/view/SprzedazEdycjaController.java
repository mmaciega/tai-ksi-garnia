package pl.tai2.view;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.controlsfx.dialog.Dialogs;
import pl.tai2.Main;
import pl.tai2.db.controller.KlientJpaController;
import pl.tai2.db.controller.KoszykJpaController;
import pl.tai2.db.controller.ProduktWKoszykuJpaController;
import pl.tai2.db.controller.SprzedazJpaController;
import pl.tai2.db.model.Klient;
import pl.tai2.db.model.Koszyk;
import pl.tai2.db.model.ProduktWKoszyku;
import pl.tai2.db.model.ProduktWKoszykuId;
import pl.tai2.db.model.Sprzedaz;

/**
 * FXML Controller class
 *
 * @author Mateusz Macięga
 */
public class SprzedazEdycjaController implements Initializable {

    @FXML
    private TextField dataPole;
    private Date data;
    @FXML
    private ComboBox<Klient> klientPoleWyboru;
    @FXML
    private TableView<ProduktWKoszyku> ksiazkaTabela;
    @FXML
    private TableColumn<ProduktWKoszyku, String> tytulKolumna;
    @FXML
    private TableColumn<ProduktWKoszyku, Short> liczbaSztukKolumna;

    @FXML
    private TextField kwotaPole;

    private Sprzedaz sprzedaz;
    private Stage scenaOkna;
    private boolean okKlikniete = false;

    public void setScenaOkna(Stage scenaOkna) {
        this.scenaOkna = scenaOkna;
    }

    public void setSprzedaz(Sprzedaz sprzedaz) {
        this.sprzedaz = sprzedaz;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ksiazkaTabela.setPlaceholder(new Label("Brak zawartości"));

        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        data = new Date();
        dataPole.setText(dt.format(data));

        tytulKolumna.setCellValueFactory(cellData -> cellData.getValue().getKsiazka().tytulProperty());
        liczbaSztukKolumna.setCellValueFactory(cellData -> cellData.getValue().liczbaSztukProperty());

        KlientJpaController klientJpaController = new KlientJpaController();
        ObservableList<Klient> klientLista = FXCollections.observableArrayList(klientJpaController.findKlientEntities());
        klientPoleWyboru.setItems(klientLista);

        klientPoleWyboru.setCellFactory((comboBox) -> {
            return new ListCell<Klient>() {
                @Override
                protected void updateItem(Klient item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(item.getImieKlienta() + " " + item.getNazwiskoKlienta());
                    }
                }
            };
        });

        klientPoleWyboru.setConverter(new StringConverter<Klient>() {
            @Override
            public String toString(Klient klient) {
                if (klient == null) {
                    return null;
                } else {
                    return klient.getImieKlienta() + " " + klient.getNazwiskoKlienta();
                }
            }

            @Override
            public Klient fromString(String klientString) {
                return null;
            }
        });

//         kwotaPole.textProperty().bind(Bindings.format("%s", Bindings.createStringBinding(() -> String.valueOf(ksiazkaTabela.getItems().size()), Bindings.size(ksiazkaTabela.getItems()))));
//        kwotaPole.textProperty().bind(Bindings.createStringBinding(() -> {
//            Double suma = ksiazkaTabela.getItems().stream().mapToDouble(p -> p.getKsiazka().getCena().doubleValue() * p.getLiczbaSztuk()).sum();
//            BigDecimal wynik = new BigDecimal(String.valueOf(suma));
//            wynik.setScale(2, BigDecimal.ROUND_HALF_UP);
//            return wynik.toString();
//        }, Bindings.size(ksiazkaTabela.getItems())));
    }

    @FXML
    public void obsluzDodajKsiazka() {
        ProduktWKoszyku produktWKoszykuTymczas = new ProduktWKoszyku();
        boolean okClicked = pokazKsiazkaEdycjaOkno(produktWKoszykuTymczas);
        if (okClicked) {
            ksiazkaTabela.getItems().add(produktWKoszykuTymczas);

            Double aktWart = Double.parseDouble(kwotaPole.getText().replaceAll(",", "."));
            Double nowa = produktWKoszykuTymczas.getLiczbaSztuk() * produktWKoszykuTymczas.getKsiazka().getCena().doubleValue();

            BigDecimal wynik = new BigDecimal(String.valueOf(aktWart + nowa));
            wynik.setScale(2, BigDecimal.ROUND_HALF_UP);

            kwotaPole.setText(String.format("%.2f", wynik.doubleValue()));
        }
    }

    @FXML
    public void obsluzUsunKsiazka() {
        int selectedIndex = ksiazkaTabela.getSelectionModel().getSelectedIndex();

        if (selectedIndex > -1) {
            ProduktWKoszyku produktWKoszykuTymczas = ksiazkaTabela.getSelectionModel().getSelectedItem();
            Double aktWart = Double.parseDouble(kwotaPole.getText().replaceAll(",", "."));
            Double nowa = produktWKoszykuTymczas.getLiczbaSztuk() * produktWKoszykuTymczas.getKsiazka().getCena().doubleValue();

            BigDecimal wynik = new BigDecimal(String.valueOf(aktWart - nowa));
            wynik.setScale(2, BigDecimal.ROUND_HALF_UP);

            kwotaPole.setText(String.format("%.2f", wynik.doubleValue()));

            ksiazkaTabela.getItems().remove(selectedIndex);
        } else {
            Dialogs.create()
                    .title("Nie wybrano sprzedaży")
                    .masthead(null)
                    .message("Proszę wybrać sprzedaż z tabeli.")
                    .showWarning();
        }
    }

    public boolean pokazKsiazkaEdycjaOkno(ProduktWKoszyku produktWKoszyku) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/SprzedazKsiazkaEdycja.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Dodaj książke do koszyka");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(scenaOkna);
            Scene scene = new Scene(page);
            dialogStage.setResizable(false);
            dialogStage.setScene(scene);

            SprzedazKsiazkaEdycjaController controller = loader.getController();
            controller.setScenaOkna(dialogStage);
            controller.setProduktWKoszyku(produktWKoszyku);
            controller.setWybraneKsiazki(ksiazkaTabela.getItems());

            dialogStage.showAndWait();

            return controller.isOkKlikniete();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @FXML
    public void obsluzOK() {
        if (czyPoprawneDaneSprzedazy()) {
            Koszyk koszyk = new Koszyk();
            koszyk.setKlient(klientPoleWyboru.getSelectionModel().getSelectedItem());

            KoszykJpaController koszykJpaController = new KoszykJpaController();
            koszykJpaController.create(koszyk);


            ProduktWKoszykuJpaController produktWKoszykuJpaController = new ProduktWKoszykuJpaController();
            List<ProduktWKoszyku> listaProduktow = ksiazkaTabela.getItems();
            for (ProduktWKoszyku p : listaProduktow) {
                ProduktWKoszykuId produktWKoszykuId = new ProduktWKoszykuId();
                produktWKoszykuId.setIdKoszyka(koszyk.getIdKoszyka());
                produktWKoszykuId.setIdKsiazki(p.getKsiazka().getIdKsiazki());
                p.setId(produktWKoszykuId);
                p.setKoszyk(koszyk);
                try {
                    produktWKoszykuJpaController.create(p);
                } catch (Exception ex) {
                    Logger.getLogger(SprzedazEdycjaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            sprzedaz.setDataSprzedazy(data);
            sprzedaz.setKoszyk(koszyk);

            okKlikniete = true;
            scenaOkna.close();
        }
    }

    private boolean czyPoprawneDaneSprzedazy() {
        String wiadomoscBledu = "";

        if (klientPoleWyboru.getSelectionModel().isEmpty()) {
            wiadomoscBledu += "Nie wybrano klienta!\n";
        }

        if (ksiazkaTabela.getItems().size() == 0) {
            wiadomoscBledu += "Nie dodano żadnej książki do koszyka!\n";
        }

        if (wiadomoscBledu.length() == 0) {
            return true;
        } else {
            Dialogs.create()
                    .title("Błędne wartości pól dla sprzedaży")
                    .masthead("Proszę poprawić niepoprawne wartości pól")
                    .message(wiadomoscBledu)
                    .showError();
            return false;
        }
    }

    @FXML
    public void obsluzAnuluj() {
        scenaOkna.close();
    }

    public boolean isOkKlikniete() {
        return okKlikniete;
    }

}
