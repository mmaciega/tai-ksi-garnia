package pl.tai2.view;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;
import pl.tai2.db.model.Adres;
import pl.tai2.db.model.Klient;

/**
 * FXML Controller class
 *
 * @author Mateusz Macięga
 */
public class KlientEdycjaController implements Initializable {

    @FXML
    private TextField imiePole;
    @FXML
    private TextField nazwiskoPole;
    @FXML
    private TextField emailPole;
    @FXML
    private TextField numerTelefonuPole;
    @FXML
    private TextField adresPole;
    @FXML
    private TextField miastoPole;
    @FXML
    private TextField kodPocztowyPole;
    @FXML
    private TextField regionPole;
    @FXML
    private TextField krajPole;

    private Klient klient;
    private Stage scenaOkna;
    private boolean okKlikniete = false;

    public void setScenaOkna(Stage scenaOkna) {
        this.scenaOkna = scenaOkna;
    }

    public void setKlient(Klient klient) {
        this.klient = klient;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    
    private boolean czyPoprawneDaneKlienta() {
        if (adresPole.getText() != null) {
            adresPole.setText(adresPole.getText().trim());
        }

        if (emailPole.getText() != null) {
            emailPole.setText(emailPole.getText().trim());
        }

        if (imiePole.getText() != null) {
            imiePole.setText(imiePole.getText().trim());
        }

        if (kodPocztowyPole.getText() != null) {
            kodPocztowyPole.setText(kodPocztowyPole.getText().trim());
        }
        
        if (krajPole.getText() != null) {
            krajPole.setText(krajPole.getText().trim());
        }
        
        if (miastoPole.getText() != null) {
            miastoPole.setText(miastoPole.getText().trim());
        }
        
        if (nazwiskoPole.getText() != null) {
            nazwiskoPole.setText(nazwiskoPole.getText().trim());
        }
        
        if (numerTelefonuPole.getText() != null) {
            numerTelefonuPole.setText(numerTelefonuPole.getText().trim());
        }
        
        if (regionPole.getText() != null) {
            regionPole.setText(regionPole.getText().trim());
        }

        String wiadomoscBledu = "";

        if (imiePole.getText() == null || imiePole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawne imie!\n";
        }
        
        if (nazwiskoPole.getText() == null || nazwiskoPole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawne nazwisko!\n";
        }
        
        final String EMAIL_PATTERN = 
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        final Pattern wzor = Pattern.compile(EMAIL_PATTERN);
        if (emailPole.getText() == null || emailPole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawny email!\n";
        } else if (!wzor.matcher(emailPole.getText()).matches()) {
            wiadomoscBledu += "Niepoprawny format emaila!\n"; 
        }
        
        if (adresPole.getText() == null || adresPole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawny adres!\n";
        }
        
        if (miastoPole.getText() == null || miastoPole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawne miasto!\n";
        }
        
        if (!numerTelefonuPole.getText().equals("")) {
            try {
                Integer wartosc = Integer.parseInt(numerTelefonuPole.getText());
                
                if (wartosc <= 0 ) {
                    wiadomoscBledu += "Niepoprawny numer telefonu (musi być dodatnią liczbą całkowitą)!\n";  
                }
            } catch (NumberFormatException e) {
                wiadomoscBledu += "Niepoprawny numer telefonu (musi być liczbą całkowitą)!\n"; 
            }
        }
        
        if (kodPocztowyPole.getText() == null || kodPocztowyPole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawny kod pocztowy!\n";
        } else if (kodPocztowyPole.getText().length() > 12) {
            wiadomoscBledu += "Niepoprawny kod pocztowy (za dużo znaków)!\n";
        }
        
        if (regionPole.getText() == null || regionPole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawny region!\n";
        }
        
        if (krajPole.getText() == null || krajPole.getText().length() == 0) {
            wiadomoscBledu += "Niepoprawny kraj!\n";
        }

        if (wiadomoscBledu.length() == 0) {
            return true;
        } else {
            Dialogs.create()
                    .title("Błędne wartości pól dla klienta")
                    .masthead("Proszę poprawić niepoprawne wartości pól")
                    .message(wiadomoscBledu)
                    .showError();
            return false;
        }
    }

    @FXML
    public void obsluzAnuluj() {
        scenaOkna.close();
    }

    @FXML
    public void obsluzOK() {
        if (czyPoprawneDaneKlienta()) {
            klient.setImieKlienta(imiePole.getText());
            klient.setNazwiskoKlienta(nazwiskoPole.getText());
            klient.setEmail(emailPole.getText());
            
            if (!numerTelefonuPole.getText().equals("")) {
                klient.setNrTelefonu(Integer.valueOf(numerTelefonuPole.getText()));
            }
            
            Adres adres = new Adres();
            adres.setLinia(adresPole.getText());
            adres.setMiasto(miastoPole.getText());
            adres.setKodPocztowy(kodPocztowyPole.getText());
            adres.setRegion(regionPole.getText());
            adres.setKraj(krajPole.getText());
            
            klient.setAdres(adres);
            
            okKlikniete = true;
            scenaOkna.close();
        }
    }

    public boolean isOkKlikniete() {
        return okKlikniete;
    }

}
