package pl.tai2.view;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import pl.tai2.Main;
import pl.tai2.db.DbManager;
import pl.tai2.db.controller.AdresJpaController;
import pl.tai2.db.controller.KlientJpaController;
import pl.tai2.db.controller.KoszykJpaController;
import pl.tai2.db.controller.KsiazkaJpaController;
import pl.tai2.db.controller.SprzedazJpaController;
import pl.tai2.db.controller.exceptions.IllegalOrphanException;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.model.Klient;
import pl.tai2.db.model.Ksiazka;
import pl.tai2.db.model.ProduktWKoszyku;
import pl.tai2.db.model.Sprzedaz;

/**
 * FXML Controller class
 *
 * @author Mateusz Macięga
 */
public class GlowneOknoController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="TableView+Columns ksiazka">
    @FXML
    private TableView<Ksiazka> ksiazkaTabela;
    @FXML
    private TableColumn<Ksiazka, String> tytulKolumna;
    @FXML
    private TableColumn<Ksiazka, String> autorzyKolumna;
    @FXML
    private TableColumn<Ksiazka, String> kategoriaKolumna;
    @FXML
    private TableColumn<Ksiazka, String> wydawnictwoKolumna;
    @FXML
    private TableColumn<Ksiazka, String> oprawaKolumna;
    @FXML
    private TableColumn<Ksiazka, Short> nrTomuKolumna;
    @FXML
    private TableColumn<Ksiazka, BigDecimal> cenaKolumna;
    @FXML
    private TableColumn<Ksiazka, String> opisKolumna;
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="TableView+Columns klient">
    @FXML
    private TableView<Klient> klientTabela;
    @FXML
    private TableColumn<Klient, String> imieKolumna;
    @FXML
    private TableColumn<Klient, String> nazwiskoKolumna;
    @FXML
    private TableColumn<Klient, String> emailKolumna;
    @FXML
    private TableColumn<Klient, Number> nrTelefonuKolumna;
    @FXML
    private TableColumn<Klient, String> adresKolumna;
    @FXML
    private TableColumn<Klient, String> miejscowoscKolumna;
    @FXML
    private TableColumn<Klient, String> kodPocztowyKolumna;
    @FXML
    private TableColumn<Klient, String> krajKolumna;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="TableView+Column sprzedaz">
    @FXML
    private TableView<Sprzedaz> sprzedazTabela;
    @FXML
    private TableColumn<Sprzedaz, Number> idKolumna;
    @FXML
    private TableColumn<Sprzedaz, Date> dataSprzedazyKolumna;
    @FXML
    private TableColumn<Sprzedaz, String> imieSprzedazKolumna;
    @FXML
    private TableColumn<Sprzedaz, String> nazwiskoSprzedazKolumna;
    @FXML
    private TableColumn<Sprzedaz, String> kwotaKolumna;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="TableView+Column produkt">
    @FXML
    private TableView<ProduktWKoszyku> produktTabela;
    @FXML
    private TableColumn<ProduktWKoszyku, String> tytulProduktKolumna;
    @FXML
    private TableColumn<ProduktWKoszyku, String> autorProduktKolumna;
    @FXML
    private TableColumn<ProduktWKoszyku, Short> liczbaSztukKolumna;
    @FXML
    private TableColumn<ProduktWKoszyku, BigDecimal> cenaProduktKolumna;
    @FXML
    private TableColumn<ProduktWKoszyku, BigDecimal> kosztProduktKolumna;
//</editor-fold>

    private Application glownaAplikacja;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicjalizacjaKsiazkaTabela();
        inicjalizacjaKlientTabela();
        inicjalizacjaSprzedazTabela();
        inicjalizacjaProduktTabela();
    }

    private void inicjalizacjaProduktTabela() {
        produktTabela.setPlaceholder(new Label("Brak zawartości"));
        tytulProduktKolumna.setCellValueFactory(cellData -> cellData.getValue().getKsiazka().tytulProperty()); 
        autorProduktKolumna.setCellValueFactory(cellData -> Bindings.createStringBinding(() -> {
            return cellData.getValue().getKsiazka().autorListProperty().stream().map((a) -> a.getImie() + " " + a.getNazwisko()).collect(Collectors.joining(", "));
        }, cellData.getValue().ksiazkaProperty(), cellData.getValue().getKsiazka().autorListProperty()));
        liczbaSztukKolumna.setCellValueFactory(cellData -> cellData.getValue().liczbaSztukProperty());
        cenaProduktKolumna.setCellValueFactory(cellData -> cellData.getValue().getKsiazka().cenaProperty());
        kosztProduktKolumna.setCellValueFactory(cellData -> Bindings.createObjectBinding(() ->  {
            Double iloczyn = cellData.getValue().getLiczbaSztuk() * cellData.getValue().getKsiazka().getCena().doubleValue();
            BigDecimal wynik = new BigDecimal(String.valueOf(iloczyn));
            wynik.setScale(2, BigDecimal.ROUND_HALF_UP);
            return wynik;
                    }, cellData.getValue().liczbaSztukProperty(), cellData.getValue().ksiazkaProperty(), cellData.getValue().getKsiazka().cenaProperty()));
    }

    private void inicjalizacjaSprzedazTabela() {
        sprzedazTabela.setPlaceholder(new Label("Brak zawartości"));
        idKolumna.setCellValueFactory(cellData -> cellData.getValue().idSprzedazyProperty());
        dataSprzedazyKolumna.setCellValueFactory(cellData -> cellData.getValue().dataSprzedazyProperty());
        imieSprzedazKolumna.setCellValueFactory(cellData -> cellData.getValue().getKoszyk().getKlient().imieKlientaProperty());
        nazwiskoSprzedazKolumna.setCellValueFactory(cellData -> cellData.getValue().getKoszyk().getKlient().nazwiskoKlientaProperty());
        kwotaKolumna.setCellValueFactory(cellData -> Bindings.createStringBinding(() -> {
            Double suma = cellData.getValue().getKoszyk().getProduktWKoszykus().stream().mapToDouble(p -> p.getKsiazka().getCena().doubleValue() * p.getLiczbaSztuk()).sum();
            BigDecimal wynik = BigDecimal.valueOf(suma);
            wynik.setScale(2, BigDecimal.ROUND_HALF_UP);
            return String.format("%.2f", wynik.doubleValue());
        }, cellData.getValue().koszykProperty()));
        
        SprzedazJpaController sprzedazJpaController = new SprzedazJpaController();
        ObservableList<Sprzedaz> sprzedazLista = FXCollections.observableArrayList(sprzedazJpaController.findSprzedazEntities());

        sprzedazTabela.setItems(sprzedazLista);
        
        sprzedazTabela.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("Wybrano " + newValue);
            
            if (newValue != null) {
                ObservableList<ProduktWKoszyku> produktLista = FXCollections.observableArrayList(newValue.getKoszyk().getProduktWKoszykus());
                produktTabela.setItems(produktLista);
            } else {    
                produktTabela.setItems(null);
            }
        });

    }

    private void inicjalizacjaKlientTabela() {
        klientTabela.setPlaceholder(new Label("Brak zawartości"));

        imieKolumna.setCellValueFactory(cellData -> cellData.getValue().imieKlientaProperty());
        nazwiskoKolumna.setCellValueFactory(cellData -> cellData.getValue().nazwiskoKlientaProperty());
        emailKolumna.setCellValueFactory(cellData -> cellData.getValue().emailProperty());
        nrTelefonuKolumna.setCellValueFactory(cellData -> cellData.getValue().nrTelefonuProperty());

        adresKolumna.setCellValueFactory(cellData -> Bindings.format("%s", cellData.getValue().getAdres().liniaProperty(), cellData.getValue().adresProperty()));
        miejscowoscKolumna.setCellValueFactory(cellData -> cellData.getValue().getAdres().miastoProperty());
        kodPocztowyKolumna.setCellValueFactory(cellData -> cellData.getValue().getAdres().kodPocztowyProperty());
        krajKolumna.setCellValueFactory(cellData -> cellData.getValue().getAdres().krajProperty());

        KlientJpaController klientJpaController = new KlientJpaController();
        ObservableList<Klient> klientLista = FXCollections.observableArrayList(klientJpaController.findKlientEntities());

        klientTabela.setItems(klientLista);
    }

    private void inicjalizacjaKsiazkaTabela() {
        ksiazkaTabela.setPlaceholder(new Label("Brak zawartości"));

        tytulKolumna.setCellValueFactory(cellData -> cellData.getValue().tytulProperty());
        autorzyKolumna.setCellValueFactory(cellData -> Bindings.createStringBinding(() -> {
            return cellData.getValue().autorListProperty().stream().map((a) -> a.getImie() + " " + a.getNazwisko()).collect(Collectors.joining(", "));
        }, cellData.getValue().autorListProperty()));
        kategoriaKolumna.setCellValueFactory(cellData -> Bindings.createStringBinding(() -> {
            return cellData.getValue().podkategoriaListProperty().stream().map((a) -> a.getKategoria().getNazwaKategorii() + " - " + a.getNazwaPodkategorii()).collect(Collectors.joining(", "));
        }, cellData.getValue().podkategoriaListProperty(), FXCollections.observableArrayList(cellData.getValue().getPodkategoriaList().stream().map((a) -> a.getKategoria().nazwaKategoriiProperty()).collect(Collectors.toList()))));
        wydawnictwoKolumna.setCellValueFactory(cellData -> cellData.getValue().getWydawnictwo().nazwaWydawnictwaProperty());
        oprawaKolumna.setCellValueFactory(cellData -> Bindings.format("%s", cellData.getValue().getOprawa() != null ? cellData.getValue().getOprawa().rodzajOprawyProperty() : "", cellData.getValue().oprawaProperty()));
        nrTomuKolumna.setCellValueFactory(cellData -> cellData.getValue().numerTomuProperty());
        cenaKolumna.setCellValueFactory(cellData -> cellData.getValue().cenaProperty());
        opisKolumna.setCellValueFactory(cellData -> cellData.getValue().opisProperty());

        KsiazkaJpaController ksiazkaJpaController = new KsiazkaJpaController();
        ObservableList<Ksiazka> ksiazkaLista = FXCollections.observableArrayList(ksiazkaJpaController.findKsiazkaEntities());

        ksiazkaTabela.setItems(ksiazkaLista);
    }

    public void setMainApp(Application glownaAplikacja) {
        this.glownaAplikacja = glownaAplikacja;
    }

    public boolean pokazKsiazkaEdycjaOkno(Ksiazka ksiazka) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/KsiazkaEdycja.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edytuj ksiazke");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(((Main) glownaAplikacja).getGlownaScena());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            KsiazkaEdycjaController controller = loader.getController();
            controller.setScenaOkna(dialogStage);
            controller.setKsiazka(ksiazka);

            dialogStage.showAndWait();

            return controller.isOkKlikniete();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean pokazKlientEdycjaOkno(Klient klient) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/KlientEdycja.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edytuj klienta");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(((Main) glownaAplikacja).getGlownaScena());
            Scene scene = new Scene(page);
            dialogStage.setResizable(false);
            dialogStage.setScene(scene);

            KlientEdycjaController controller = loader.getController();
            controller.setScenaOkna(dialogStage);
            controller.setKlient(klient);
            
            dialogStage.showAndWait();

            return controller.isOkKlikniete();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean pokazSprzedazEdycjaOkno(Sprzedaz sprzedaz) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/SprzedazEdycja.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edytuj sprzedaż");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(((Main) glownaAplikacja).getGlownaScena());
            Scene scene = new Scene(page);
            dialogStage.setResizable(false);
            dialogStage.setScene(scene);


            SprzedazEdycjaController controller = loader.getController();
            controller.setScenaOkna(dialogStage);
            controller.setSprzedaz(sprzedaz);

            dialogStage.showAndWait();

            return controller.isOkKlikniete();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean pokazOprawaEdycjaOkno() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/OprawaEdycja.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edytuj oprawy");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(((Main) glownaAplikacja).getGlownaScena());
            Scene scene = new Scene(page);
            dialogStage.setResizable(false);
            dialogStage.setScene(scene);

            OprawaEdycjaController controller = loader.getController();
            controller.setScenaOkna(dialogStage);

            dialogStage.showAndWait();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @FXML
    private void obsluzOProgramieMenu() {
        Dialogs.create()
                .title("O programie")
                .masthead(null)
                .message("Technologie Aplikacji Internetowych\nProjekt numer 2\nAutor: Mateusz Macięga\nWersja: 1.0\n2015")
                .showInformation();
    }

    @FXML
    private void obsluzDodawanieKsiazki() {
        Ksiazka ksiazkaTymczas = new Ksiazka();
        boolean okClicked = pokazKsiazkaEdycjaOkno(ksiazkaTymczas);
        if (okClicked) {
            ksiazkaTabela.getItems().add(ksiazkaTymczas);

            KsiazkaJpaController ksiazkaJpaController = new KsiazkaJpaController();
            ksiazkaJpaController.create(ksiazkaTymczas);
        }
    }
    
    @FXML
    private void obsluzEdytowanieKsiazki() {
        Ksiazka zaznaczonaKsiazka = ksiazkaTabela.getSelectionModel().getSelectedItem();
        if (zaznaczonaKsiazka != null) {
            boolean okClicked = pokazKsiazkaEdycjaOkno(zaznaczonaKsiazka);
            if (okClicked) {
            KsiazkaJpaController ksiazkaJpaController = new KsiazkaJpaController();
                try {
                    ksiazkaJpaController.edit(zaznaczonaKsiazka);
                } catch (Exception ex) {
                    Logger.getLogger(OprawaEdycjaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            Dialogs.create()
                .title("Nie wybrano książki")
                .masthead(null)
                .message("Proszę wybrać ksiażke.")
                .showWarning();
        }
    }

    @FXML
    private void obsluzUsuwanieKsiazki() {
        int selectedIndex = ksiazkaTabela.getSelectionModel().getSelectedIndex();
        Ksiazka selectedKsiazka = ksiazkaTabela.getSelectionModel().getSelectedItem();

        if (selectedIndex > -1) {

            Action odpowiedz = Dialogs.create()
                    .title("Potwierdz usunięcie ksiązki")
                    .masthead(null)
                    .message("Czy na pewno chcesz usunać wybraną książke?")
                    .showConfirm();

            if (odpowiedz == Dialog.Actions.YES) {

                ksiazkaTabela.getItems().remove(selectedIndex);

                KsiazkaJpaController ksiazkaJpaController = new KsiazkaJpaController();
                try {
                    ksiazkaJpaController.destroy(selectedKsiazka.getIdKsiazki());
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(GlowneOknoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                SprzedazJpaController sprzedazJpaController = new SprzedazJpaController();
                ObservableList<Sprzedaz> sprzedazLista = FXCollections.observableArrayList(sprzedazJpaController.findSprzedazEntities());

                sprzedazTabela.setItems(sprzedazLista);
                sprzedazTabela.getSelectionModel().clearSelection();
            }
        } else {
            Dialogs.create()
                    .title("Nie wybrano ksiażki")
                    .masthead(null)
                    .message("Proszę wybrać książkę z tabeli do usunięcia.")
                    .showWarning();
        }
    }

    @FXML
    private void obsluzDodawanieKlienta() {
        Klient klientTymczas = new Klient();
        boolean okClicked = pokazKlientEdycjaOkno(klientTymczas);
        if (okClicked) {
            klientTabela.getItems().add(klientTymczas);

            AdresJpaController adresJpaController = new AdresJpaController();
            adresJpaController.create(klientTymczas.getAdres());
            
            KlientJpaController klientJpaController = new KlientJpaController();
            klientJpaController.create(klientTymczas);
        }
    }

    @FXML
    private void obsluzUsuwanieKlienta() {
        int selectedIndex = klientTabela.getSelectionModel().getSelectedIndex();
        Klient selectedKlient = klientTabela.getSelectionModel().getSelectedItem();

        if (selectedIndex > -1) {

            Action odpowiedz = Dialogs.create()
                    .title("Potwierdz usunięcie klienta")
                    .masthead(null)
                    .message("Czy na pewno chcesz usunać wybranego klienta?")
                    .showConfirm();

            if (odpowiedz == Dialog.Actions.YES) {
                try {
                    klientTabela.getItems().remove(selectedIndex);
                    
                    KlientJpaController klientJpaController = new KlientJpaController();
                    klientJpaController.destroy(selectedKlient.getIdKlienta());
                    
                    AdresJpaController adresJpaController = new AdresJpaController();
                    adresJpaController.destroy(selectedKlient.getAdres().getIdAdresu());
                    
                    SprzedazJpaController sprzedazJpaController = new SprzedazJpaController();
                    ObservableList<Sprzedaz> sprzedazLista = FXCollections.observableArrayList(sprzedazJpaController.findSprzedazEntities());

                    sprzedazTabela.setItems(sprzedazLista);
                    sprzedazTabela.getSelectionModel().clearSelection();
                } catch (IllegalOrphanException | NonexistentEntityException ex) {
                    Logger.getLogger(GlowneOknoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            Dialogs.create()
                    .title("Nie wybrano klienta")
                    .masthead(null)
                    .message("Proszę wybrać klienta z tabeli do usunięcia.")
                    .showWarning();
        }
    }
    
    @FXML
    private void obsluzDodawanieSprzedazy() {
        Sprzedaz sprzedazTymczas = new Sprzedaz();
        boolean okClicked = pokazSprzedazEdycjaOkno(sprzedazTymczas);
        if (okClicked) {
            sprzedazTabela.getItems().add(sprzedazTymczas);
            
            SprzedazJpaController sprzedazJpaController = new SprzedazJpaController();
            sprzedazJpaController.create(sprzedazTymczas);
        }
    }
    
    @FXML
    private void obsluzUsuwanieSprzedazy() {
        int selectedIndex = sprzedazTabela.getSelectionModel().getSelectedIndex();
        Sprzedaz selectedSprzedaz = sprzedazTabela.getSelectionModel().getSelectedItem();

        if (selectedIndex > -1) {
            Action odpowiedz = Dialogs.create()
                    .title("Potwierdz usunięcie sprzedaży")
                    .masthead(null)
                    .message("Czy na pewno chcesz usunać wybraną sprzedaż?")
                    .showConfirm();

            if (odpowiedz == Dialog.Actions.YES) {
                try {
                    sprzedazTabela.getItems().remove(selectedIndex);

                    SprzedazJpaController sprzedazJpaController = new SprzedazJpaController();
                    sprzedazJpaController.destroy(selectedSprzedaz.getIdSprzedazy());
                    
                    KoszykJpaController koszykJpaController = new KoszykJpaController();
                    koszykJpaController.destroy(selectedSprzedaz.getKoszyk().getIdKoszyka());
                } catch (NonexistentEntityException | IllegalOrphanException ex) {
                    Logger.getLogger(GlowneOknoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            Dialogs.create()
                    .title("Nie wybrano sprzedaży")
                    .masthead(null)
                    .message("Proszę wybrać sprzedaż z tabeli do usunięcia.")
                    .showWarning();
        }
    }

    @FXML
    private void obsluzEdycjaOprawaMenu() {
        pokazOprawaEdycjaOkno();
    }

    @FXML
    private void obsluzZakonczMenu() {
        DbManager.getInstance().shutDown();
        Platform.exit();
        System.exit(0);
    }
}
