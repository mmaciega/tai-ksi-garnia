package pl.tai2.view;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import pl.tai2.Main;
import pl.tai2.db.controller.OprawaJpaController;
import pl.tai2.db.controller.exceptions.NonexistentEntityException;
import pl.tai2.db.model.Ksiazka;
import pl.tai2.db.model.Oprawa;

/**
 * FXML Controller class
 *
 * @author Mateusz Macięga
 */
public class OprawaEdycjaController implements Initializable {

    @FXML
    private TableView<Oprawa> oprawaTabela;
    
    @FXML
    private TableColumn<Oprawa, String> nazwaKolumna;
    
    private Stage scenaOkna;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        oprawaTabela.setPlaceholder(new Label("Brak zawartości"));
        
        OprawaJpaController oprawaJpaController = new OprawaJpaController();
        ObservableList<Oprawa> oprawaLista = FXCollections.observableArrayList(oprawaJpaController.findOprawaEntities());
        oprawaTabela.setItems(oprawaLista);
        
        nazwaKolumna.setCellValueFactory(cellData -> cellData.getValue().rodzajOprawyProperty());
    }    
    
    public void setScenaOkna(Stage scenaOkna) {
        this.scenaOkna = scenaOkna;
    }
   
    @FXML
    public void obsluzDodaj() {
        Oprawa oprawaTymczas = new Oprawa();
        boolean okClicked = pokazOprawaEdycjaOkno(oprawaTymczas);
        if (okClicked) {
            oprawaTabela.getItems().add(oprawaTymczas);

            OprawaJpaController oprawaJpaController = new OprawaJpaController();
            oprawaJpaController.create(oprawaTymczas);
        }
    }
    
    public boolean pokazOprawaEdycjaOkno(Oprawa oprawa) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/OprawaUstawienie.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edytuj oprawę");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(scenaOkna);
            Scene scene = new Scene(page);
            dialogStage.setResizable(false);
            dialogStage.setScene(scene);

            OprawaUstawienieController controller = loader.getController();
            controller.setScenaOkna(dialogStage);
            controller.setOprawa(oprawa);

            dialogStage.showAndWait();

            return controller.isOkKlikniete();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    @FXML
    public void obsluzUsun() {
        int selectedIndex = oprawaTabela.getSelectionModel().getSelectedIndex();
        
        if(selectedIndex > -1) {
            oprawaTabela.getItems().remove(selectedIndex);
        } else {
            Dialogs.create()
                .title("Nie wybrano oprawy")
                .masthead(null)
                .message("Proszę wybrać oprawę z tabeli.")
                .showWarning();
        }
    }
    
    @FXML
    public void obsluzEdytuj() {
        Oprawa zaznaczonaOprawa = oprawaTabela.getSelectionModel().getSelectedItem();
        if (zaznaczonaOprawa != null) {
            boolean okClicked = pokazOprawaEdycjaOkno(zaznaczonaOprawa);
            if (okClicked) {
                OprawaJpaController oprawaJpaController = new OprawaJpaController();
                try {
                    oprawaJpaController.edit(zaznaczonaOprawa);
                } catch (Exception ex) {
                    Logger.getLogger(OprawaEdycjaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            Dialogs.create()
                .title("Nie wybrano oprawy")
                .masthead(null)
                .message("Proszę wybrać oprawe.")
                .showWarning();
        }
    }
    
    @FXML 
    public void obsluzZamknij() {
        scenaOkna.close();
    }
    
    @FXML
    public void obsluzUsuniecie() {
        int selectedIndex = oprawaTabela.getSelectionModel().getSelectedIndex();
        Oprawa selectedItem = oprawaTabela.getSelectionModel().getSelectedItem();
        
        if(selectedIndex > -1) {
            List<Ksiazka> ksiazkaList = selectedItem.getKsiazkaList();
            
            Action odpowiedz = Dialogs.create()
                    .title("Potwierdz usunięcie oprawy")
                    .masthead(null)
                    .message("Czy na pewno chcesz usunać wybraną oprawe? Spowoduje to zmodyfikowanie " 
                            + String.valueOf(ksiazkaList.size()) + " książek")
                    .showConfirm();

            if (odpowiedz == Dialog.Actions.YES) {
                oprawaTabela.getItems().remove(selectedIndex);

                OprawaJpaController oprawaJpaController = new OprawaJpaController();
                try {
                    oprawaJpaController.destroy(selectedItem.getIdOprawy());
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(OprawaEdycjaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            Dialogs.create()
                .title("Nie wybrano oprawy")
                .masthead(null)
                .message("Proszę wybrać oprawę z tabeli.")
                .showWarning();
        }
    }
    
}
